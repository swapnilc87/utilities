use searchdb;

CREATE TABLE searchdb.query_filters_raw_daily
(
session_id STRING,
client_ts BIGINT,
filter_value STRING,
filter_name STRING,
user_query STRING,
time_delta DOUBLE
)
PARTITIONED BY (data_date DATE)

STORED AS ORC
LOCATION 'hdfs:///user/hadoop/search_db_tables/query_filters'
;



