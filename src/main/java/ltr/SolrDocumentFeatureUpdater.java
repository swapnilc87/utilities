package ltr;

import com.google.common.util.concurrent.RateLimiter;
import crawl.CrawlUtil;
import feature.CategoricalFeature;
import feature.Constants;
import feature.Feature;
import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class SolrDocumentFeatureUpdater implements Constants {

    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new FileReader("/Users/300032675/searchWorkspace/codebase/Utilities/src/main/resources/style_dict_json_20180625.json"));
//        BufferedReader reader = new BufferedReader(new FileReader("/Users/300032675/searchWorkspace/codebase/Utilities/src/main/resources/sample_product_features.json"));
        String line = null;
        RateLimiter rateLimiter = RateLimiter.create(5);
        String url = "http://localhost:8984/solr/sprod/update?versions=true";
        Map<String,String> featureFieldMap = getFeatureFieldMap();
        JSONArray jsonArray = new JSONArray();
        int count = 0;
        int SLICE = 500;
        DateTime start = DateTime.now();
        while ((line = reader.readLine()) != null) {
            count++;
            if(count%SLICE ==0) {
                rateLimiter.acquire();
                long l = DateTime.now().getMillis() - start.getMillis();
                start = DateTime.now();
                System.out.println("flushing : "+count+" "+l);
                CrawlUtil.curlPOST(url, jsonArray);
                System.out.println("flush complete");
//                System.out.println(jsonArray);
                jsonArray = new JSONArray();

            }
            String[] parts = line.trim().split("\t");
            String styleId = parts[0];
            JSONObject inputJson = new JSONObject(parts[1]);
            Iterator<String> keyIterator = inputJson.keys();
            JSONObject doc = new JSONObject();
            while(keyIterator.hasNext()) {
                String featureName = keyIterator.next();
                String fieldName = featureFieldMap.get(featureName);
                if(fieldName == null) {
//                   System.err.println(featureName);
                   continue;
                }
                if(fieldName.startsWith("global_attr")) {
                    continue;
                }
                JSONObject val = new JSONObject();
                String value = inputJson.getString(featureName);
                try {
                    float fValue = Float.valueOf(value);
                    val.put("set", fValue);

                }catch (Exception e) {
                    if(featureName.startsWith("feature_")) {
                        continue;
                    }
                    val.put("set",value);
                }
                doc.put(fieldName,val);

            }
            doc.put("id", "0_style_" + styleId);
            doc.put("storeId", "2297");
            jsonArray.put(doc);
//            CrawlUtil.curlPOST(url, jsonArray);
        }
        if(jsonArray.length()!=0) {
            CrawlUtil.curlPOST(url, jsonArray);
            System.out.println("flushing end");
//            System.out.println(jsonArray);
        }

        reader.close();
    }

    public static Map<String, String> getFeatureFieldMap() throws Exception {
        Map<String, String> featureFieldMap = new HashMap<>();
        String dataStr = FeatureUploader.readFile("/Users/300032675/searchWorkspace/codebase/Utilities/src/main/resources/ltr_features_new.json");
        JSONObject baseObject = new JSONObject(dataStr);
        JSONArray featuresArray = baseObject.getJSONArray("features");

        String featureMapstr = FeatureUploader.readFile("/Users/300032675/searchWorkspace/codebase/Utilities/resources/feature_map.json");
        JSONObject featureMapJson = new JSONObject(featureMapstr);
        Map<String,String> featureNameMap = new HashMap<>();


        for (int i = 0; i < featuresArray.length(); i++) {
            JSONObject featureJson = featuresArray.getJSONObject(i);
            String name = featureJson.getString("feature_name");
            String type = featureJson.getString("type");
            String subType = featureJson.getString("subType");
            if ("P".equalsIgnoreCase(type)) {
//                System.out.println(subType + " " + name);
                Feature f = Feature.getFeature(subType, name,featureNameMap).loadParamsFromJson(featureJson.getJSONObject("params"));
//                String fieldName = (String) (f.getParams().get(FIELD) == null ? f.getParams().get(FIELD_NAME) : f.getParams().get(FIELD));
                String fieldName = (String) (f.getParams().get(FIELD));

                if(f instanceof CategoricalFeature) {
                    CategoricalFeature categoricalFeature = (CategoricalFeature) f;
                    fieldName = categoricalFeature.getFieldName();
//                    List<String> list = categoricalFeature.getValues();
//                    for(String val : list) {
//                        String featureName = name+"_"+val.replaceAll("\\s","_");
//                        featureFieldMap.put(featureName,fieldName);
//                    }
//
                }
//                else {
                    if (fieldName != null) {
                        featureFieldMap.put(name, fieldName);
                    }
//                }
            }
        }
        featureFieldMap.put("feature_age_group","global_attr_age_group");
        featureFieldMap.put("feature_season","global_attr_season");
        return featureFieldMap;
    }
}
