package ltr;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class FeatureComparer {

    public static void main(String[] args) throws Exception{
        String modelData = FeatureUploader.readFile("/Users/300032675/sample_index_data/first_cut_model_new.json");
        JSONObject modelJson = new JSONObject(modelData);

        List<String> featuresList = new ArrayList<>();

        JSONArray featuresArray = modelJson.getJSONArray("features");
        for(int i=0;i<featuresArray.length();i++) {
            JSONObject obj = featuresArray.getJSONObject(i);
            String feature = obj.getString("name");
            featuresList.add(feature);
        }
        Collections.sort(featuresList);
        System.out.println(String.join(";",featuresList));

        List<String> fList = new ArrayList<>();
        String featureData = FeatureUploader.readFile("/Users/300032675/sample_index_data/first_cut_features_new.json");
        JSONArray featureJson = new JSONArray(featureData);
        for(int i=0;i<featureJson.length();i++) {
            JSONObject obj = featureJson.getJSONObject(i);
            String feature = obj.getString("name");
            fList.add(feature);
        }

        Collections.sort(fList);
        System.out.println(String.join(";",fList));
    }
}
