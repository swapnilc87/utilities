package ltr;

import ltr.constants.LTRConstants;
import org.apache.solr.client.solrj.*;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.request.schema.SchemaRequest;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.StringUtils;
import org.apache.solr.common.params.SolrParams;
import org.apache.solr.common.util.NamedList;
import org.json.JSONObject;

import java.io.IOException;

public class FeatureStoreDownloader {

    public static void main(String[] args) throws Exception{
        FeatureStoreDownloader featureStoreDownloader = new FeatureStoreDownloader();
        featureStoreDownloader.getFeaturesFromFeatureStore(null);
    }


    public JSONObject getFeaturesFromFeatureStore(String featureStore) {
        if(StringUtils.isEmpty(featureStore)) {
            featureStore = LTRConstants.DEFAULT_FEATURE_STORE;
        }
        SchemaRequest schemaRequest = new SchemaRequest();
        schemaRequest.setPath("schema/feature-store");
        SolrClient client = new HttpSolrClient.Builder("http://localhost:8984/solr/sprod").build();
        try {
            NamedList<Object> resp = client.request(schemaRequest);
            System.out.println(resp);
        } catch (SolrServerException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;

    }
}
