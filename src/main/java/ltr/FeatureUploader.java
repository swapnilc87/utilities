package ltr;

import crawl.CrawlUtil;
import org.apache.solr.common.util.JsonRecordReader;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.*;

public class FeatureUploader {
    public static void main(String[] args) throws Exception{
        String baseUrl = "http://s25:8984/solr/sprod/schema/feature-store";
//        String baseUrl = "http://s19:8984/solr/sprod/schema/feature-store";
//        String baseUrl = "http://fcp-charles3:8984/solr/sprod/schema/feature-store";
//        String baseUrl = "http://s23:8984/solr/sprod/schema/feature-store";

//        String dataStr = readFile("/Users/300032675/sample_index_data/first_cut_features_new.json");
//        String dataStr = readFile("/Users/300032675/sample_index_data/single_tree_features.json");
//        String dataStr = readFile("/Users/300032675/sample_index_data/prod_features_upload_modified_qd.json");
//        String dataStr = readFile("/Users/300032675/sample_index_data/prod_model_31Oct/prod_features_upload_modified_qd.json");
//        String dataStr = readFile("/Users/300032675/sample_index_data/prod_model_15_nov/prod_features_upload_modified_qd.json");
        String dataStr = readFile("/Users/300032675/sample_index_data/model_20190407_200kb/prod_features_upload_modified_qd.json");
//        String dataStr = readFile("/Users/300032675/sample_index_data/prod_features_upload.json");
//        String dataStr = readFile("/Users/300032675/sample_index_data/sprod_efi_features.json");

        JSONArray data = new JSONArray(dataStr);

        CrawlUtil.curlPUT(baseUrl,data);

    }

    public static String readFile(String filename) {
        String result = "";
        try {
            BufferedReader br = new BufferedReader(new FileReader(filename));
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();
            while (line != null) {
                sb.append(line);
                line = br.readLine();
            }
            result = sb.toString();
        } catch(Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
