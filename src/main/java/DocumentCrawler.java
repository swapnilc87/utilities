import com.google.common.base.Joiner;
import crawl.CrawlUtil;
import org.json.JSONArray;
import org.json.JSONObject;
import product.Style;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class DocumentCrawler {
    public static void main(String[] args) throws Exception{
        String queryStr = "polo tshirts";
        String queries[] = {"polo tshirts","solid polo tshirts","jeans","dresses","a line dresses","kurtas","tops","black tops"};
        String baseUrl = "http://localhost:7500/search-service/searchservice/search/getresults/";

        for(String query: queries) {
            List<Style> list = getProducts(query, baseUrl);
//        System.out.println(list.size());
            System.out.println(query + "\t" + Joiner.on(",").join(list.stream().map(d -> d.getStyleid()).collect(Collectors.toList())));
        }
    }

    private static List<Style> getProducts(String query, String baseUrl) throws Exception{
        String baseRequestStr="{\n" +
                "\"return_docs\": false,\n" +
                "\"is_facet\": false,\n" +
                "  \"serverInfo\":true,\n" +
                "  \"user\": {\n" +
                "    \"login\":\"0007b817.0f9d.40d4.b762.504e96a98635F2OtiMQBUQ\"\n" +
                "  },\n" +
                "  \n" +
                "  \"rows\":101\n" +
                "}";
        JSONObject jsonObject = new JSONObject(baseRequestStr);
        jsonObject.put("query",query);
        JSONObject response = CrawlUtil.curlPOSTJson(baseUrl,jsonObject);
        List<Style> styleList = new ArrayList<>();

        if(response!=null) {
            System.out.println(response.getJSONObject("response").getString("solrQuery"));
            JSONArray products = response.getJSONObject("response").getJSONArray("products");
            for(int i = 0; i<products.length();i++) {
                JSONObject obj = products.getJSONObject(i);
                Style style = new Style(obj);
                styleList.add(style);
            }
        }
        return styleList;
    }
}
