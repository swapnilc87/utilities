package builder;

import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

public class UrlBuilder {
    private static final String base = "http://%s:%s/%s?";
    Map<String,Object> params = new HashMap<String, Object>();

    public UrlBuilder(String host,String port,String subService) {
        subService = StringUtils.trimToEmpty(subService);
        data.append(String.format(base, host,port,subService));
    }

    public UrlBuilder(String base) {
        data.append(base);
    }

    public UrlBuilder(String host,String port) {
        this(host,port,null);
    }

    StringBuffer data = new StringBuffer();


    public UrlBuilder addParam(String name,Object value) {
        if(name!=null && value!=null) {
            params.put(name,value);
        }
        return this;
    }

    public UrlBuilder start(int startValue) {
        params.put("start",startValue);
        return  this;
    }
    public UrlBuilder rows(int value) {
        params.put("rows",value);
        return  this;
    }
    public UrlBuilder query(String query) {
        params.put("q",query==null?"*:*":query);
        return  this;
    }


    public String buildUrl() {
        if(params.isEmpty()) {
            return data.toString();
        }

        for(String name: params.keySet()) {
            data.append(name).append("=").append(params.get(name)).append("&");
        }

        return data.toString();
    }

}
