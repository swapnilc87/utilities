package product;

import org.json.JSONObject;

public class Style {
    Long styleid;
    String product;

    public Style(JSONObject obj) {
        try {
            this.styleid = obj.getLong("productId");
            this.product = obj.getString("product");
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Long getStyleid() {
        return styleid;
    }

    public void setStyleid(Long styleid) {
        this.styleid = styleid;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }
}
