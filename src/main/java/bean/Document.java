package bean;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Document {
    String brand;
    Double score;
    boolean isNew;

    public Document(String brand, Double score, boolean isNew) {
        this.brand = brand;
        this.score = score;
        this.isNew = isNew;
    }

    @Override
    public String toString() {
        return  brand + " || " +
                 + score +
                " || " +
                 isNew
                ;
    }

    public static void main(String[] args) {
        List<Document> list = new ArrayList<>();

        list.add(new Document("nike",0.9,false));
        list.add(new Document("nike",0.86,false));
        list.add(new Document("puma",0.85,false));
        list.add(new Document("nike",0.6,true));
        list.add(new Document("addidas",0.54,false));
        list.add(new Document("nike",0.3,false));
        list.add(new Document("nike",0.2,false));
        list.add(new Document("fila",0.18,false));

        System.out.println(list.stream().map(d->d.brand).collect(Collectors.toList()));

        Collections.sort(list, new Comparator<Document>() {
            @Override
            public int compare(Document d1, Document d2) {
                if(d1.brand.equalsIgnoreCase(d2.brand)) {
                    return -1;
                }else {
                    if(!d1.isNew && d2.isNew) {
                        return -1;
                    }
                }

                return 0;
            }
        });

        System.out.println(list.stream().map(d->d.brand).collect(Collectors.toList()));
    }
}
