package bean;

import org.json.JSONObject;

public class CDocument implements Constants{
    Integer styleid;
    String product;
    double score;

    public CDocument(JSONObject json) {
        try {
            if (json.has(STYLEID)) {
                this.styleid = json.getInt(STYLEID);
            }
            if (json.has(PRODUCT)) {
                this.product = json.getString(PRODUCT);
            }
            if (json.has(SCORE)) {
                this.score = json.getDouble(SCORE);
            }

        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Integer getStyleid() {
        return styleid;
    }

    public void setStyleid(Integer styleid) {
        this.styleid = styleid;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }
}
