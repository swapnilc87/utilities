package crawl;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class CrawlUtil {

    public static JSONObject readJsonFromUrl(String url) throws IOException, JSONException {
        InputStream is = new URL(url).openStream();
        return new JSONObject(IOUtils.toString(is, "UTF-8"));
    }

    public static boolean curlPUT(String url, JSONArray data) {

        try {

            HttpClient httpClient = HttpClientBuilder.create().build();
            HttpPut postRequest = new HttpPut(
                    url);

            StringEntity input = new StringEntity(data.toString());
            input.setContentType("application/json");
            postRequest.setEntity(input);

            HttpResponse response = httpClient.execute(postRequest);

            if (response.getStatusLine().getStatusCode() != 200) {
                return false;
            }

            BufferedReader br = new BufferedReader(
                    new InputStreamReader((response.getEntity().getContent())));

            String output;
            System.out.println("Output from Server .... \n");
            while ((output = br.readLine()) != null) {
                System.out.println(output);
            }

            httpClient.getConnectionManager().shutdown();
        } catch (MalformedURLException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();

        }
        return true;
    }

    public static boolean curlDelete(String url) {

        try {

            HttpClient httpClient = HttpClientBuilder.create().build();
            HttpDelete deleteRequest = new HttpDelete(
                    url);

            HttpResponse response = httpClient.execute(deleteRequest);

            if (response.getStatusLine().getStatusCode() != 200) {
                return false;
            }

            BufferedReader br = new BufferedReader(
                    new InputStreamReader((response.getEntity().getContent())));

            String output;
            System.out.println("Output from Server .... \n");
            while ((output = br.readLine()) != null) {
                System.out.println(output);
            }

            httpClient.getConnectionManager().shutdown();
        } catch (MalformedURLException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();

        }
        return true;
    }

    public static boolean curlPOST(String url, Object data) {
        try {

            HttpClient httpClient = HttpClientBuilder.create().build();
            HttpPost postRequest = new HttpPost(
                    url);

            StringEntity input = new StringEntity(data.toString());
            input.setContentType("application/json");
            postRequest.setEntity(input);
            HttpResponse response = httpClient.execute(postRequest);

            if (response.getStatusLine().getStatusCode() != 200) {
                System.out.println("status code="+response.getStatusLine().getStatusCode());
                System.out.println(response.getStatusLine().getReasonPhrase());
                System.out.println("===="+data);
                return false;
            }

            BufferedReader br = new BufferedReader(
                    new InputStreamReader((response.getEntity().getContent())));

            String output;
            System.out.println("Output from Server .... \n");
            while ((output = br.readLine()) != null) {
//                System.out.println(output);
            }

            httpClient.getConnectionManager().shutdown();
        } catch (MalformedURLException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();

        }
        return true;
    }


    public static JSONObject curlPOSTJson(String url, Object data) throws JSONException {
        return curlPOSTJsonWithHeaders(url,data,new HashMap<>());
    }
    public static JSONObject curlPOSTJsonWithHeaders(String url, Object data, Map<String,String> headerMap) throws JSONException {
        StringBuffer buf = new StringBuffer();
        try {

            HttpClient httpClient = HttpClientBuilder.create().build();
            HttpPost postRequest = new HttpPost(
                    url);

            StringEntity input = new StringEntity(data.toString());
            input.setContentType("application/json");
            postRequest.setEntity(input);

            if(headerMap!=null && headerMap.size() >0) {
                for(Map.Entry<String,String> entry : headerMap.entrySet()) {
                    postRequest.setHeader(entry.getKey(),entry.getValue());
                }
            }
//            postRequest.setHeader("Accept","application/json");
//            postRequest.setHeader("x-mynt-ctx","storeid=2297;uidx=0007b817.0f9d.40d4.b762.504e96a98635F2OtiMQBUQ");
            HttpResponse response = httpClient.execute(postRequest);

            if (response.getStatusLine().getStatusCode() != 200) {
                System.out.println("status code="+response.getStatusLine().getStatusCode());
                System.out.println(response.getStatusLine().getReasonPhrase());
                System.out.println("===="+data);
                return null;
            }

            BufferedReader br = new BufferedReader(
                    new InputStreamReader((response.getEntity().getContent())));

            String output;

            System.out.println("Output from Server .... \n");
            while ((output = br.readLine()) != null) {
//                System.out.println(output);
                buf.append(output);
            }

            httpClient.getConnectionManager().shutdown();

        } catch (MalformedURLException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();

        }
        return new JSONObject(buf.toString());
    }

}
