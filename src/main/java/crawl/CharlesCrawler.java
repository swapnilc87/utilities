package crawl;

import builder.UrlBuilder;
import com.google.common.util.concurrent.RateLimiter;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

public class CharlesCrawler {


    public static void main(String[] args) throws Exception {
        UrlBuilder urlBuilder = new UrlBuilder("localhost", "7500", "search-service/searchservice/search/getresults/");
        Map<String, String> controlHeader = getMyntraCharlesHeaders();
        Map<String, String> variantHeader = getMyntraCharlesHeaders();
        AtomicInteger success = new AtomicInteger(0);
        AtomicInteger failure = new AtomicInteger(0);

//        List<String> queries = getQueries("/Users/300032675/notebooks/LTR/ltr_queries.csv");
        List<String> queries = getQueries("/Users/300032675/Downloads/DAqueries.csv");

//        variantHeader.put("ltrv1", "variant");
        System.out.println("Finished reading queries");
        ExecutorService executorService = Executors.newFixedThreadPool(1);
        RateLimiter rateLimiter = RateLimiter.create(5);
        for (String query : queries) {
            executorService.submit(() -> {
                rateLimiter.acquire();
                try {
                    JSONObject data = getBody();

                    System.out.println(query + " - " + (success.get() + failure.get()));
                    data.put("query", query);
                    data.put("rows", 48);

                    JSONObject response = CrawlUtil.curlPOSTJsonWithHeaders(urlBuilder.buildUrl(), data, variantHeader);

                    if (response != null && response.has("response") && response.getJSONObject("response").has("products") && response.getJSONObject("response").getJSONArray("products").length() > 0) {
                        if (success.incrementAndGet() > 1000) {
                            System.out.println("success");
                        }
                    } else {
                        if (failure.incrementAndGet() > 50) {
                            System.out.println("failure");
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    if (failure.incrementAndGet() > 50) {
                        System.out.println("failure");
                    }
                }

            });

        }

        System.out.println("efficiency : " + ((success.doubleValue() / (success.doubleValue() + failure.doubleValue()))));
        System.out.println(success);
        System.out.println(failure);
    }

    private static List<String> getQueries(String fileName) throws Exception {
        List<String> list = new ArrayList<>();

        BufferedReader reader = new BufferedReader(new FileReader(fileName));

        String line = null;

        while ((line = reader.readLine()) != null) {
            if (!list.contains(line.trim())) {
                list.add(line.trim());
            }
        }
        return list;
    }

    private static JSONObject getBody() {
        JSONObject data = new JSONObject();
        try {

            data.put("return_docs", false);
            data.put("is_facet", false);
            data.put("serverInfo", true);
            data.put("serverInfo", true);
//            data.put("user", new JSONObject());
//            data.getJSONObject("user").put("login", "0007b817.0f9d.40d4.b762.504e96a98635F2OtiMQBUQ");
        } catch (Exception e) {

        }
        return data;
    }

    public static Map<String, String> getMyntraCharlesHeaders() {
        Map<String, String> headers = new HashMap<>();
        headers.put("Accept", "application/json");
        headers.put("Content-Type", "application/json");
        headers.put("x-mynt-ctx", "storeid=2297");
        return headers;
    }
}
