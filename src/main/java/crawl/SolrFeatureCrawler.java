package crawl;

import builder.UrlBuilder;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.net.URLEncoder;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class SolrFeatureCrawler {
    private static final int BATCH_SIZE = 10;

    public static void main(String[] args) throws Exception {

        int start = 0;
        UrlBuilder builder = new UrlBuilder("http://localhost:8984/solr/sprod/select?")
//                    .addParam("fq",URLEncoder.encode("count_options_availbale: [ 1 TO * ]"))
//                    .addParam("fq",URLEncoder.encode("styletype:P"))
                .addParam("fl", "product,id,[features]")
                .start(start).rows(BATCH_SIZE).query(null);


        System.out.println("processing :" + start);
        System.out.println(builder.buildUrl());
        JSONObject response = CrawlUtil.readJsonFromUrl(builder.buildUrl());
        JSONArray docs = response.getJSONObject("response").getJSONArray("docs");

        for (int i = 0; i < docs.length(); i++) {
            JSONObject document = docs.getJSONObject(i);
            Iterator<String> iterator = document.sortedKeys();
            Set<String> inValidKeys = getInvalidKeys(iterator, "_version_", "Inventory_for", "inventory_for", "name_exact", "coverage_");
            for (String invalidKey : inValidKeys) {
                document.remove(invalidKey);
            }
            String delim = ",";
            if (i == docs.length() - 1) {
                delim = "";
            }
        }
    }


    public static Set<String> getInvalidKeys(Iterator<String> iterator, String... inValidPatterns) {
        String key = null;
        Set<String> inValidKeys = new HashSet<>();
        while (iterator.hasNext()) {
            key = iterator.next();
            for (String invalidPattern : inValidPatterns) {
                if (key.matches(invalidPattern) || key.contains(invalidPattern)) {
                    inValidKeys.add(key);
                }
            }
        }
        return inValidKeys;
    }


}
