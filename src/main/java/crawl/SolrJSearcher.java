package crawl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrRequest;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocumentList;

public class SolrJSearcher {
    public static void main(String[] args) throws IOException, SolrServerException {
        SolrClient client = new HttpSolrClient.Builder("http://localhost:8984/solr/sprod").build();
        String queryString = "sweaters";
        SolrQuery query = new SolrQuery();
        query.setQuery(queryString);
//        query.addFilterQuery("cat:electronics","store:amazon.com");
        query.setFields("styleId", "[features efi.user_query="+queryString+"]");
        query.setStart(0);
        query.setRows(10);


        query.set("df","product");
        query.setRequestHandler("/query");
//        query.set("defType", "edismax");
        System.out.println(query.setShowDebugInfo(true));
        System.out.println(query);
        System.out.println("....");
        QueryResponse response = client.query(query,SolrRequest.METHOD.POST);
        SolrDocumentList results = response.getResults();
        for (int i = 0; i < results.size(); ++i) {
            String value = (String) results.get(i).getFieldValue("[features]");
            String[] features = value.split(",");
//            System.out.println(value+"-=-=-="+features.length);
            List<String> featureSet = new ArrayList<>();
            for(int index=0;index<features.length;index++) {
//                System.out.println(features[index]);
                String arr[] = features[index].split("=");
                featureSet.add((index+1)+":"+arr[1]);
            }
            System.out.println(queryString+"\t"+String.join(" ",featureSet));
        }
    }
}