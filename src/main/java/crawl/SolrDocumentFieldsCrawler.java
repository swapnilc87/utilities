package crawl;

import builder.UrlBuilder;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.net.URLEncoder;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class SolrDocumentFieldsCrawler {
    public static void main(String[] args) throws Exception{

        int BATCH_SIZE=10000;
        for(int start=3390000;start<4400000;start+=BATCH_SIZE) {
            BufferedWriter writer = new BufferedWriter(new FileWriter("/Users/300032675/sample_index_data/dump/sprod_fields"+(start/BATCH_SIZE)+".json"));
            writer.write("[");
            writer.newLine();
            UrlBuilder builder = new UrlBuilder("http://s8:8984/solr/sprod/select?")
//                    .addParam("fq",URLEncoder.encode("count_options_availbale: [ 1 TO * ]"))
//                    .addParam("fq",URLEncoder.encode("styletype:P"))
                    .addParam("fl","product,id,typename,descr,global_attr_brand,global_attr_gender,global_attr_article_type,global_attr_sub_category,global_attr_master_category")
                    .start(start).rows(BATCH_SIZE).query(null);

            System.out.println("processing :"+start);
            System.out.println(builder.buildUrl());
            JSONObject response = CrawlUtil.readJsonFromUrl(builder.buildUrl());
            JSONArray docs = response.getJSONObject("response").getJSONArray("docs");

            for (int i = 0; i < docs.length(); i++) {
                JSONObject document = docs.getJSONObject(i);
                Iterator<String> iterator = document.sortedKeys();
                Set<String> inValidKeys = getInvalidKeys(iterator, "_version_", "Inventory_for", "inventory_for", "name_exact","coverage_");
                for (String invalidKey : inValidKeys) {
                    document.remove(invalidKey);
                }
                String delim = ",";
                if (i == docs.length() - 1) {
                    delim = "";
                }
                writer.write(document + delim);
                writer.newLine();
                if (i % 100 == 0) {
                    writer.flush();
                }
            }
            writer.write("]");
            writer.close();
        }


    }

    public static Set<String> getInvalidKeys(Iterator<String> iterator, String...inValidPatterns) {
        String key =null;
        Set<String> inValidKeys = new HashSet<>();
        while(iterator.hasNext()) {
            key = iterator.next();
            for(String invalidPattern: inValidPatterns) {
                if(key.matches(invalidPattern) || key.contains(invalidPattern)) {
                    inValidKeys.add(key);
                }
            }
        }
        return inValidKeys;
    }



}
