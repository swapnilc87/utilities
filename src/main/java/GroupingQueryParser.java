import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.response.Group;
import org.apache.solr.client.solrj.response.GroupCommand;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.params.CommonParams;

public class GroupingQueryParser {

    public static void main(String[] args) throws Exception {
        SolrQuery query = new SolrQuery();
        query.add(CommonParams.Q, "global_attr_article_type:kurtas");
        query.set("group", "true");
        query.set("group.field", "Sleeve_Length_article_attr"); //replace "manu_exact" with your field name here
        query.set("group.ngroups", "true");
        query.set("group.limit", "-1");
        query.setFields("styleid,product,score,global_attr_brand,Sleeve_Length_article_attr".split(","));
        query.addFilterQuery("styletype:P","global_attr_article_type : (\"kurtas\")");
        SolrClient solrClient = new HttpSolrClient.Builder("http://localhost:8984/solr").build();


        QueryResponse response = null;
        try {
            response = solrClient.query("sprod", query); //replace "techproducts" with your core name here
        } catch (Exception e) {
//log the exception
            e.printStackTrace();
        }

        if (response.getGroupResponse() != null) {
            for (GroupCommand groupCommand : response.getGroupResponse().getValues()) {
/**
 * groupCommand.getName(); returns the field by which grouping was done
 * groupCommand.getMatches(); returns the number of documents matched in totality
 * groupCommand.getNGroups(); returns the number of groups identified
 * groupCommand.getValues(); returns the List of Groups returned
 **/
                for (Group group : groupCommand.getValues()) { //This loop traverses through each group
                    System.out.println(group.getGroupValue());

/**
 * group.getGroupValue(); returns the common value that each document shares inside this group
 * group.getResult(); returns the SolrDocumentList retrieved for this group
 */
                    for (SolrDocument doc : group.getResult()) {
/**
 * doc.getFieldValue(FIELD_NAME); returns the value or collection of values for a given FIELD_NAME
 * doc.getFirstValue(FIELD_NAME); returns the first value for a field if it is FIELD_NAME
 */
                    }
                }
            }
        }
    }
}
