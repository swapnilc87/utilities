package gmail;

import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;

import java.util.Arrays;
import java.util.Properties;
import javax.mail.*;
import javax.mail.search.*;

public class MailReader {


    public static final String USER_NAME = "USER_NAME";
    public static final String GMAIL_APP_PASS = "GMAIL_APP_PASS";

    public static void main(String[] args) throws Exception{
        System.out.println("hello");
        System.out.println();

//        String host = "pop.gmail.com";// change accordingly
        String host = "IMAP.gmail.com";// change accordingly
//        String mailStoreType = "pop3";
        String mailStoreType = "imaps";
        String username = System.getenv(USER_NAME);// change accordingly
        String password = System.getenv(GMAIL_APP_PASS);// change accordingly

//        check(host, mailStoreType, username, password);
        readUnreadMessage(host, mailStoreType, username, password);
    }


    // function to make connection and get mails from server known as "Pop" server
    public static void check(String host, String storeType, String user, String password) {
        try {

            //create properties field
            Properties properties = new Properties();

            properties.put("mail.pop3.host", host);
//            properties.put("mail.pop3.port", "995");
            properties.put("mail.pop3.port", "993");
            properties.put("mail.pop3.starttls.enable", "true");

            Session emailSession = Session.getDefaultInstance(properties);

            //create the POP3 store object and connect with the pop server
            Store store = emailSession.getStore("pop3s");

            store.connect(host, user, password);

            //create the folder object and open it
            Folder emailFolder = store.getFolder("inbox");

            emailFolder.open(Folder.READ_ONLY);

            // retrieve the messages from the folder in an array and print it
            Message[] messages = emailFolder.getMessages();
            System.out.println("messages.length---" + messages.length);

            for (int i = 0, n = messages.length; i < n; i++) {
                Message message = messages[i];


                Object obj = message.getContent();
                Multipart mp = (Multipart) obj;
                BodyPart bp = mp.getBodyPart(0);


                System.out.println("---------------------------------");
                System.out.println("Email Number " + (i + 1));
                System.out.println("Subject: " + message.getSubject());
                System.out.println("From: " + message.getFrom()[0]);
                System.out.println("To: " + message.getAllRecipients().toString());
                System.out.println("Received Date:" + message.getReceivedDate());
                System.out.println("Text: " + bp.getContent().toString());
            }

            //close the store and folder objects
            emailFolder.close(false);
            store.close();

        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        } catch (MessagingException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void readUnreadMessage(String host, String storeType, String user, String password) throws Exception{

        Session session = Session.getDefaultInstance(new Properties( ));
        Store store = session.getStore("imaps");
        store.connect(host, 993, user, password);
        Folder inbox = store.getFolder( "ON_CALL/NOT_LIVE" );
        inbox.open( Folder.READ_ONLY );

        // Fetch unseen messages from inbox folder
        Message[] messages = inbox.search(
                new FlagTerm(new Flags(Flags.Flag.SEEN), false));

        // Sort messages from recent to oldest
        Arrays.sort( messages, (m1, m2 ) -> {
            try {
                return m2.getSentDate().compareTo( m1.getSentDate() );
            } catch ( MessagingException e ) {
                throw new RuntimeException( e );
            }
        } );

        HtmlCleaner htmlCleaner = new HtmlCleaner();
        System.out.println("Number of unread messages:"+messages.length);
        for ( Message message : messages ) {
            Object obj = message.getContent();
            Multipart mp = (Multipart) obj;
            BodyPart bp = mp.getBodyPart(0);
            BodyPart htmlBp = mp.getBodyPart(1);


            System.out.println("---------------------------------");
            System.out.println("Subject: " + message.getSubject());
            System.out.println("From: " + message.getFrom()[0]);
            System.out.println("To: " + message.getAllRecipients().toString());
            System.out.println("Received Date:" + message.getReceivedDate());
//            System.out.println("Text: " + bp.getContent().toString());

            System.out.println("Text: " + htmlBp.getContent());
            TagNode tagNode = htmlCleaner.clean(htmlBp.getContent().toString());

            System.out.println("Text cleaned: " + tagNode.getText().toString());
            System.out.println("######");
        }
    }
}
