package solr.indexer;

import ltr.FeatureUploader;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.common.SolrInputDocument;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


public class SolrFeatureIndexer {

    public static void main(String[] args) throws Exception{

        HttpSolrClient.Builder builder = new HttpSolrClient.Builder();

        HttpSolrClient client = builder.withBaseSolrUrl("http://localhost:8984/solr").build();

        String styleSJson = FeatureUploader.readFile("/Users/300032675/Downloads/small_style.json");
        JSONObject jsonObject = new JSONObject(styleSJson);

        JSONArray array = jsonObject.getJSONArray("styleFeatures");

        for (int i = 0; i <array.length() ; i++) {
            JSONObject style = array.getJSONObject(i);
            Integer styleId = style.getInt("styleId");
            JSONObject featureValueMap = style.getJSONObject("featureValueMap");
            Iterator<String> iterator = featureValueMap.keys();


            SolrInputDocument sdoc = new SolrInputDocument();
            sdoc.addField("id","0_style_"+styleId);
            sdoc.addField("storeId", 2297);
            System.out.println(styleId);

            while (iterator.hasNext()) {
                String featureName = iterator.next();
                Object fieldValue = featureValueMap.get(featureName);
                Map<String,Object> fieldModifier = new HashMap<>(1);
                fieldModifier.put("set",fieldValue);
                sdoc.addField(featureName,fieldModifier);
            }
//            sdoc.addField("feature_2", 2.0);  // add the map as the field value

            client.add( "sprod",sdoc );  // send it to the solr server

        }

        // create the SolrJ client

        client.commit();
// create the document

        client.close();  // shutdown client before we exit
    }


}
