package feature;

import org.json.JSONException;
import org.json.JSONObject;

public class BooleanFeature extends Feature {

    Boolean value;

    public BooleanFeature(String name) {
        super(name);
    }


    @Override
    public String toString() {
        return "BooleanFeature{" +
                "Value=" + value +
                ", name='" + name + '\'' +
                '}';
    }

    @Override
    public String getClassName() {
        return "org.apache.solr.ltr.feature.BooleanFeature";
    }

    @Override
    public Feature loadParamsFromJson(JSONObject input) {
        if(input==null) {
            return this;
        }

        try {
            String parameterName = input.getString(PARAMETER_NAME);
            if(parameterName!=null) {
                params.put(VALUE,"${"+parameterName+"}");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return this;
    }


}
