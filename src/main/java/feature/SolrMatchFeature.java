package feature;

import org.json.JSONArray;
import org.json.JSONObject;

public class SolrMatchFeature extends Feature{
    String matchType;
    public SolrMatchFeature(String name) {
        super(name);
    }
    public SolrMatchFeature(String name,String matchType) {
        super(name);
        this.matchType = matchType;
    }

    @Override
    public String getClassName() {
        return "org.apache.solr.ltr.feature.SolrFeature";
    }

    @Override
    public Feature loadParamsFromJson(JSONObject input) {
        if(input!=null) {
            try {
                String fieldName = input.getString(FIELD_NAME);
                String var = null;
                if (input.has(PARAMETER_NAME)) {
                    var = "${" + input.getString(PARAMETER_NAME) + "}";
                } else if (input.has(VALUE_STR)) {
                    var = input.getString(VALUE_STR);
                }

                switch (matchType) {
                    case "exact":
                        JSONArray jsonArray = new JSONArray();
                        jsonArray.put("{!field f=" + fieldName + "}" + "('"+var+"')");
                        params.put("fq", jsonArray);
                        break;
                    default:
                        params.put("q", "{!dismax qf=" + fieldName + "}" + var);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return this;
    }
}
