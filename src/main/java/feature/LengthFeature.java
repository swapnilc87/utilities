package feature;

import org.json.JSONException;
import org.json.JSONObject;

public class LengthFeature extends Feature {


    public LengthFeature(String name) {
        super(name);
    }

    public String getClassName() {
        return "org.apache.solr.ltr.feature.LengthFeature";
    }

    @Override
    public Feature loadParamsFromJson(JSONObject input) {
        if (input != null) {

            try {
                String parameterName = input.getString(PARAMETER_NAME);
                if (parameterName != null) {
                    params.put(VALUE, "${" + parameterName + "}");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return this;
    }


}
