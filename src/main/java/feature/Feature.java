package feature;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class Feature implements Constants{
    String name;

    Map<String,Object> params = new HashMap<>();

    public Feature(String name) {
        this.name = name;
    }

    public abstract String getClassName();

    public static Feature getFeature(String type,String name,Map<String,String> featureNameMap) {
        String shortenedName = featureNameMap.get(name);
        switch (type) {
            case "length":
                return new LengthFeature(shortenedName);
            case "boolean":
                return new BooleanFeature(shortenedName);
            case "QPvalue" :
                return new QPValueFeature(shortenedName);
            case "match_boolean":
                return new SolrMatchFeature(shortenedName,"exact");
            case "match_partial":
                return new SolrMatchFeature(shortenedName,"partial");
            case "categorical_boolean":
                return new CategoricalFeature(name,featureNameMap);
            case "Pvalue":
                return new SolrFieldFeature(shortenedName);

        };

        return null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<String, Object> getParams() {
        return params;
    }

    public void setParams(Map<String, Object> params) {
        this.params = params;
    }
    public JSONObject paramsToJson() throws JSONException {
        JSONObject object = new JSONObject();
        if(params!=null && !params.isEmpty()) {
            for(Map.Entry<String,Object> entry : params.entrySet()) {
                object.put(entry.getKey(),entry.getValue());
            }
        }

        return object;
    }

    public List<JSONObject> toJsonObject() {
        List<JSONObject> list = new ArrayList<>();
        JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.put(NAME, getName());
            jsonObject.put(CLASS,getClassName());
            jsonObject.put(PARAMS,paramsToJson());
        }catch (Exception e) {
            e.printStackTrace();
        }
        list.add(jsonObject);
        return list;
    }

    public abstract Feature loadParamsFromJson(JSONObject input);

}
