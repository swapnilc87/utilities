package feature;

import org.json.JSONArray;
import org.json.JSONObject;

public class SolrFieldFeature extends Feature {
    public SolrFieldFeature(String name) {
        super(name);
    }

    @Override
    public String getClassName() {
//        return "org.apache.solr.ltr.feature.FieldValueFeature";
        return "org.apache.solr.ltr.feature.SolrFeature";
    }

    @Override
    public Feature loadParamsFromJson(JSONObject input) {
        if(input!=null) {
            try {
                String fieldName = input.getString(FIELD_NAME);
//                params.put(FIELD,fieldName);
                params.put(Q,"{!func} field("+fieldName+")");

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return this;
    }


}
