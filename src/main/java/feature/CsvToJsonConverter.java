package feature;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileReader;

public class CsvToJsonConverter implements Constants{
    public static void main(String[] args) throws Exception{
//        BufferedReader reader = new BufferedReader(new FileReader("/Users/300032675/searchWorkspace/codebase/Utilities/src/main/resources/features.csv"));
        BufferedReader reader = new BufferedReader(new FileReader("/Users/300032675/searchWorkspace/codebase/Utilities/src/main/resources/features_ltr.csv"));
        String line;
        JSONObject featuresJson = new JSONObject();
        JSONArray featuresJsonArray = new JSONArray();
        while((line=reader.readLine())!=null) {
            String parts[] = line.split(",");

            String name = parts[0].toLowerCase();
            String type = parts[1];
            String sub_type = parts[2];
            String parameterName = parts.length>3?parts[3].toLowerCase():null;
            String fieldName = parts.length>4?parts[4].toLowerCase():null;
            JSONObject  fJson = new JSONObject();
            JSONObject  params = new JSONObject();

            fJson.put("feature_name",name);
            fJson.put("subType",sub_type);
            fJson.put("type",type);
            fJson.put("params",params);

            if("QP".equalsIgnoreCase(type)&&"QPvalue".equalsIgnoreCase(sub_type)) {
                params.put(FEATURE_NAME, toNull(parameterName));
            }else if("categorical_boolean".equalsIgnoreCase(sub_type)){
                JSONArray jsonArray = new JSONArray();
                String[] vals = parts[5].split(":");
                for (String str:
                        vals) {
                    jsonArray.put(str);

                }
                params.put(VALUES,jsonArray);
            }
            else {
                params.put(PARAMETER_NAME, toNull(parameterName));

            }
            params.put(FIELD_NAME,toNull(fieldName));
            featuresJsonArray.put(fJson);
        }

        featuresJson.put("features",featuresJsonArray);
        System.out.println(featuresJson.toString());

        reader.close();
    }

    private static Object toNull(String parameterName) {
        return "".equals(parameterName)?null:parameterName;
    }
}
