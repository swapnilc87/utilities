package feature;

import org.json.JSONException;
import org.json.JSONObject;

public class QPValueFeature extends Feature {
    public QPValueFeature(String name) {
        super(name);
    }

    @Override
    public String getClassName() {
        return "org.apache.solr.ltr.feature.QDIndirectSolrFeature";
    }

    @Override
    public Feature loadParamsFromJson(JSONObject input) {
        if(input==null) {
            return this;
        }
        params.put("featureName",getName());
        params.put(Q,"{!func} field(styleid)");
        return this;
    }
}
