package feature;

public interface Constants {
    public final String CLASS = "class";
    public final String NAME = "name";
    public final String PARAMS = "params";
    public final String VALUE = "value";
    public final String VALUES = "values";
    public final String VALUE_STR = "value_str";
    public final String MATCH_TYPE = "match_type";
    public final String PARAMETER_NAME = "parameter_name";
    public final String FEATURE_NAME = "feature_name";
    public final String FIELD_NAME = "field_name";
    public final String FIELD = "field";
    public final String Q = "q";
}
