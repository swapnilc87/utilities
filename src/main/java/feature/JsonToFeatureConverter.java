package feature;

import ltr.FeatureUploader;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class JsonToFeatureConverter implements Constants{


    public static void main(String[] args) throws Exception{
//        String dataStr = FeatureUploader.readFile("/Users/300032675/searchWorkspace/codebase/Utilities/src/main/resources/features.json");
//        String dataStr = FeatureUploader.readFile("/Users/300032675/searchWorkspace/codebase/Utilities/src/main/resources/features_ltr.json");
//        String dataStr = FeatureUploader.readFile("/Users/300032675/searchWorkspace/codebase/Utilities/src/main/resources/ltr_features_new.json");
//        String dataStr = FeatureUploader.readFile("/Users/300032675/searchWorkspace/codebase/Utilities/src/main/resources/features_ltr_second.json");
//        String dataStr = FeatureUploader.readFile("/Users/300032675/searchWorkspace/codebase/Utilities/src/main/resources/single_tree_features.json");
//        String dataStr = FeatureUploader.readFile("/Users/300032675/sample_index_data/prod_model_31Oct/feature_json_gender.txt");
//        String dataStr = FeatureUploader.readFile("/Users/300032675/sample_index_data/prod_model_15_feb/feature_json_20190210.json");
        String dataStr = FeatureUploader.readFile("/Users/300032675/sample_index_data/model_20190407_200kb/feature_json_model_rf_20190407_200kb.json");
//        String featureMapstr = FeatureUploader.readFile("/Users/300032675/sample_index_data/prod_model_31Oct/feature_map.json");
//        String featureMapstr = FeatureUploader.readFile("/Users/300032675/sample_index_data/prod_model_15_feb/feature_map_20190210.json");
        String featureMapstr = FeatureUploader.readFile("/Users/300032675/sample_index_data/model_20190407_200kb/feature_map_model_rf_20190407_200kb.json");
        JSONObject featureMapJson = new JSONObject(featureMapstr);
        Map<String,String> featureNameMap = new HashMap<>();
        for (Iterator it = featureMapJson.keys(); it.hasNext(); ) {
            String key = (String)it.next();
            featureNameMap.put(key,featureMapJson.getString(key));
        }
        JSONObject baseObject = new JSONObject(dataStr);
        JSONArray featuresArray = baseObject.getJSONArray("features");
        JSONArray featureList = new JSONArray();
        for (int i = 0; i < featuresArray.length(); i++) {
            JSONObject featureJson = featuresArray.getJSONObject(i);
            String name = featureJson.getString("feature_name");
            String type = featureJson.getString("type");
            String subType = featureJson.getString("subType");
            System.out.println(subType+" "+name);
            if(featureNameMap.get(name)==null && !"categorical_boolean".equalsIgnoreCase(subType)) {
                System.err.println(featureJson.getString("feature_name")+"\t"+subType+"\t"+type);
//                System.err.println(featureJson);
                continue;
            }
            Feature f = Feature.getFeature(subType,name,featureNameMap).loadParamsFromJson(featureJson.getJSONObject("params"));
            List<JSONObject> list = f.toJsonObject();
            for(JSONObject obj : list) {
                featureList.put(obj);
            }
        }

        BufferedWriter writer = new BufferedWriter(new FileWriter("/Users/300032675/sample_index_data/model_20190407_200kb/prod_features_upload_modified_qd.json"));
        writer.write(featureList.toString());
        System.out.println(featureList.toString());

        writer.flush();
        writer.close();

    }
}
