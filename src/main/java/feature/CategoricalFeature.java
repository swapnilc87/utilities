package feature;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class CategoricalFeature extends Feature {
    List<String> values = new ArrayList<>();
    String fieldName;
    Map<String,String> featureNameMap = null;

    public CategoricalFeature(String name,Map<String,String> featureNameMap) {
        super(name);
        this.featureNameMap = featureNameMap;
    }

    @Override
    public String getClassName() {
        return "org.apache.solr.ltr.feature.SolrFeature";
    }

    @Override
    public Feature loadParamsFromJson(JSONObject input) {
        if (input != null) {

            try {
                fieldName = input.getString(FIELD_NAME);
                JSONArray valuesJson = input.getJSONArray(VALUES);
                for (int i = 0; i < valuesJson.length(); i++) {
                    String val = valuesJson.getString(i);
                    if (!values.contains(val)) {
                        values.add(val);
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return this;
    }

    public List<String> getValues() {
        return values;
    }

    public void setValues(List<String> values) {
        this.values = values;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    @Override
    public List<JSONObject> toJsonObject() {
        List<JSONObject> list = new ArrayList<>();
        for (String value : getValues()) {
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put(VALUE_STR,value);
                jsonObject.put(FIELD_NAME,fieldName);

                list.add(new SolrMatchFeature(featureNameMap.get(getName()+"_"+value.replaceAll("\\s","_")),"exact").loadParamsFromJson(jsonObject).toJsonObject().get(0));
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        return list;
    }
}
