import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class SolrSql {


    public static void main(String[] args) throws Exception{
        String zkHost = "localhost:2181/sprod";
        Connection con = null;
        try {
            con = DriverManager.getConnection("jdbc:solr://" + zkHost + "?collection=sprod&aggregationMode=map_reduce&numWorkers=2");
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT product FROM sprod limit 10");

            while(rs.next()) {
                String a_s = rs.getString("product");
                System.out.println(a_s);
            }
        } finally {
        }
    }
}
