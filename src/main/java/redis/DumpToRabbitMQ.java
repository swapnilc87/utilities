package redis;

import crawl.CrawlUtil;
import ltr.FeatureUploader;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.Iterator;

public class DumpToRabbitMQ {

    public static void main(String[] args) throws Exception {
        String fileName = "/Users/300032675/searchWorkspace/codebase/Utilities/resources/query_style_reddit_json_20180625_copy.json";
        if (args != null && args.length > 0) {
            fileName = args[0];
        }
        String baseUrl = "http://localhost:9881/bolt/v1//index/styles/ltr/";

//        String data = FeatureUploader.readFile("/Users/300032675/searchWorkspace/codebase/Utilities/src/main/resources/query_style_reddit_json_20180625.json");
        String data = FeatureUploader.readFile(fileName);

        JSONObject object = new JSONObject(data);

        Iterator<String> iterator = object.keys();

        JSONObject baseJson = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        baseJson.put("queryProductFeatures", jsonArray);
        int count = 1;

        int batchSize =100;
        while (iterator.hasNext()) {
            if (jsonArray.length() % batchSize == batchSize-1) {
                System.out.println(baseJson);
                CrawlUtil.curlPOSTJson(baseUrl, baseJson);
                baseJson = new JSONObject();
                jsonArray = new JSONArray();
                baseJson.put("queryProductFeatures", jsonArray);
            }
            String query = iterator.next();
            JSONObject queryJson = new JSONObject();
            queryJson.put("query", query);
            JSONArray rabbitMqFormatFeatures = getRabbitMqFormat(object.getJSONObject(query));
            queryJson.put("features", rabbitMqFormatFeatures);
            if(rabbitMqFormatFeatures.length()!=0) {
                jsonArray.put(queryJson);
            }


//            addData(jedisCluster,query,object.getJSONObject(query));
//            System.out.println(object.getJSONObject(query).toString());
        }
        if(jsonArray.length()!=0) {
            CrawlUtil.curlPOSTJson(baseUrl, baseJson);
        }

    }

    public static JSONArray getRabbitMqFormat(JSONObject jsonObject) throws JSONException {
        JSONArray jsonArray = new JSONArray();
        Iterator<String> iterator = jsonObject.keys();
        while (iterator.hasNext()) {
            JSONObject object = new JSONObject();
            String featureName = iterator.next();
            object.put("featureName",featureName);
            object.put("styleValueMap",jsonObject.getJSONObject(featureName));
            if(jsonObject.getJSONObject(featureName).length()!=0) {
                jsonArray.put(object);
            }
        }

        return jsonArray;
    }
}
