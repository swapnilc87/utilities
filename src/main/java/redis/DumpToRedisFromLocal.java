package redis;

import ltr.FeatureUploader;
import org.json.JSONException;
import org.json.JSONObject;
import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.JedisCluster;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class DumpToRedisFromLocal {

    public static void main(String[] args) throws Exception{
        String fileName = "/Users/300032675/searchWorkspace/codebase/Utilities/src/main/resources/query_style_reddit_json_20180625_copy.json";
        if(args!=null && args.length > 0) {
            fileName = args[0];
        }
        Set<HostAndPort> hostAndPortSet = new HashSet<>();
        hostAndPortSet.add(new HostAndPort("d7lgpredis.myntra.com", 7001));
        JedisCluster jedisCluster = new JedisCluster(hostAndPortSet);

//        String data = FeatureUploader.readFile("/Users/300032675/searchWorkspace/codebase/Utilities/src/main/resources/query_style_reddit_json_20180625.json");
        String data = FeatureUploader.readFile(fileName);

        JSONObject  object  = new JSONObject(data);

        Iterator<String> iterator = object.keys();
        while(iterator.hasNext()) {
            String query = iterator.next();
            addData(jedisCluster,query,object.getJSONObject(query));
//            System.out.println(object.getJSONObject(query).toString());
        }

    }

    public static void addData(JedisCluster jedisCluster,String qry, JSONObject data) {
        try {
            jedisCluster.set(qry,data.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
