package redis;

import com.lambdaworks.redis.RedisURI;
import com.lambdaworks.redis.cluster.RedisClusterClient;
import com.lambdaworks.redis.cluster.api.StatefulRedisClusterConnection;
import com.lambdaworks.redis.codec.Utf8StringCodec;
import org.apache.commons.lang.StringUtils;

public class LettuceRedisLocalFetch {
    static String redisHost = "127.0.0.1";
    static int redisPort = 30002;
    static long timeout = 1000000000l;
    public static void main(String[] args) throws Exception {
       LettuceRedisLocalFetch localFetch = new LettuceRedisLocalFetch();
        StatefulRedisClusterConnection connection = localFetch.masterConn(localFetch.createRedisClusterClient());

        System.out.println(connection.sync().hgetall("kurtas"));;
        System.out.println(connection);
    }


    private StatefulRedisClusterConnection<String, String> masterConn(RedisClusterClient clusterClient){
        return clusterClient.connect(SnappyCompressor.wrap(new StringCodec()));
    }

    private StatefulRedisClusterConnection<String, String> uncompressedConn(RedisClusterClient clusterClient){
        return clusterClient.connect(new StringCodec());
    }
    private synchronized RedisClusterClient createRedisClusterClient() throws Exception {
        if (StringUtils.isEmpty(redisHost)) {
            throw new Exception("Could not setup redis:cluster: could not find the key redis.cluster.host.");
        }
        RedisURI clusterUri = new RedisURI();
        clusterUri.setHost(redisHost);
        clusterUri.setPort(redisPort);
        clusterUri.setTimeout(timeout);
        return RedisClusterClient.create(clusterUri);
    }
}
