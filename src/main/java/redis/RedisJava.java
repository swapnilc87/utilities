package redis;

import org.json.JSONException;
import org.json.JSONObject;
import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisCluster;
import redis.clients.jedis.JedisPool;

import java.util.HashSet;
import java.util.Set;

public class RedisJava {
    public static void main(String[] args) {
        //Connecting to Redis server on localhost
        Set<HostAndPort> hostAndPortSet = new HashSet<>();
        hostAndPortSet.add(new HostAndPort("localhost", 30001));
        hostAndPortSet.add(new HostAndPort("localhost", 30002));
        hostAndPortSet.add(new HostAndPort("localhost", 30003));
        hostAndPortSet.add(new HostAndPort("localhost", 30004));
        hostAndPortSet.add(new HostAndPort("localhost", 30005));
        hostAndPortSet.add(new HostAndPort("localhost", 30006));
        JedisCluster jedisCluster = new JedisCluster(hostAndPortSet);
        String qry = "nike football shoes";
//        String qry = "IZOD Sweater";
//        addData(jedisCluster,qry);
        System.out.println(jedisCluster.get(qry));
//      jedisCluster.del("click_rate");

        System.out.println("Connection to server sucessfully");
        //check whether server is running or not

    }

    public static void addData(JedisCluster jedisCluster,String qry) {
        try {
            JSONObject jsonObject = new JSONObject();
            JSONObject clickRate = new JSONObject();
            clickRate.put("523055", 13.0);
//            clickRate.put("2468490", 5.0);

            JSONObject atcRate = new JSONObject();
//            atcRate.put("3077045", 2.5);
            atcRate.put("523055", 1.1);

            jsonObject.put("click_rate",clickRate);
            jsonObject.put("atc_rate",atcRate);
            System.out.println(jsonObject);
            jedisCluster.set(qry,jsonObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
//      jsonObject.put("sweater",)
    }
} 