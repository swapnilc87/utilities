package redis;

import com.lambdaworks.redis.RedisURI;
import com.lambdaworks.redis.cluster.RedisClusterClient;
import com.lambdaworks.redis.cluster.api.StatefulRedisClusterConnection;
import ltr.FeatureUploader;
import org.apache.commons.lang.StringUtils;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class DumpToRedisHset {

    static String redisHost = "127.0.0.1";
    static int redisPort = 30001;
    static long timeout = 1000000000l;
    public static void main(String[] args) throws Exception {
        DumpToRedisHset localFetch = new DumpToRedisHset();
        StatefulRedisClusterConnection connection = localFetch.masterConn(localFetch.createRedisClusterClient());

        String query = "kurtas";
        String jsonData = FeatureUploader.readFile("/Users/300032675/searchWorkspace/codebase/Utilities/resources/"+query+".json");
        JSONObject object = new JSONObject(jsonData);

        Iterator<String> iterator  = object.keys();

        Map<String,String> map = new HashMap<>();
        while(iterator.hasNext()) {
            String key = iterator.next();
            JSONObject value = object.getJSONObject(key);
            map.put(key,value.toString());
        }
        System.out.println(query);
            System.out.println(connection.sync().hmset("ltr_"+query, map));

        System.out.println(connection.sync().hgetall("ltr_"+"kurtas").keySet());
        System.out.println(connection.sync().hgetall("ltr_"+"tshirts").keySet());
    }


    private StatefulRedisClusterConnection<String, String> masterConn(RedisClusterClient clusterClient){
        return clusterClient.connect(SnappyCompressor.wrap(new StringCodec()));
    }

    private StatefulRedisClusterConnection<String, String> uncompressedConn(RedisClusterClient clusterClient){
        return clusterClient.connect(new StringCodec());
    }
    private synchronized RedisClusterClient createRedisClusterClient() throws Exception {
        if (StringUtils.isEmpty(redisHost)) {
            throw new Exception("Could not setup redis:cluster: could not find the key redis.cluster.host.");
        }
        RedisURI clusterUri = new RedisURI();
        clusterUri.setHost(redisHost);
        clusterUri.setPort(redisPort);
        clusterUri.setTimeout(timeout);
        return RedisClusterClient.create(clusterUri);
    }
}
