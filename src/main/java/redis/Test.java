package redis;

import java.io.IOException;

import com.fasterxml.jackson.databind.ObjectMapper;
import ltr.FeatureUploader;

public class Test {

    public static void main(String[] args) throws IOException {

        ObjectMapper mapper = new ObjectMapper();

        String str = FeatureUploader.readFile("/Users/300032675/searchWorkspace/codebase/Utilities/resources/tshirts.json");

        int iter = 50;
        System.out.println("Entering the experiment");
        long startTime = System.currentTimeMillis();
        Object obj =null;
        for (int i = 0; i < iter; i++)
            obj = mapper.readValue(str, Object.class);
        long endTime = System.currentTimeMillis();
        long duration = (endTime - startTime);
        duration /= iter*1.0;
        System.out.println(duration);


    }

}