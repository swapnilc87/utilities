package redis;

import com.lambdaworks.redis.codec.RedisCodec;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;

public class StringCodec implements RedisCodec<String, String> {

	private static Logger logger = LoggerFactory.getLogger(StringCodec.class);
	private Charset charset = Charset.forName("UTF-8");

	@Override
	public String decodeKey(ByteBuffer bytes) {
		return charset.decode(bytes).toString();
	}

	@Override
	public String decodeValue(ByteBuffer bytes) {
		try {
			return new String(bytes.array(), charset);
		} catch (Exception e) {
			logger.error("Error decoding!", e);
			return null;
		}
	}

	@Override
	public ByteBuffer encodeKey(String key) {
		return charset.encode(key);
	}

	@Override
	public ByteBuffer encodeValue(String value) {
		return ByteBuffer.wrap(value.getBytes(charset));
	}
}