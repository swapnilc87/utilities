package redis;

import ltr.FeatureUploader;
import org.json.JSONArray;
import org.json.JSONObject;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.util.List;

public class LoadRedisAndPost {

    public static void main(String[] args) throws Exception{
//        String fileData = "{}";
        String fileData = new String(Files.readAllBytes(Paths.get("/Users/300032675/redis_data.json")));

        JSONObject data = new JSONObject(fileData);
//
//        JSONArray queryProductFeatures = data.getJSONArray("queryProductFeatures");
//
//        for (int i = 0; i < queryProductFeatures.length(); i++) {
//            JSONObject qpFeature = queryProductFeatures.getJSONObject(i);
//            System.out.println(qpFeature.getString("query"));
//        }


        RuntimeMXBean runtimeMxBean = ManagementFactory.getRuntimeMXBean();
        List<String> arguments = runtimeMxBean.getInputArguments();
        System.out.println(arguments);
        System.out.println(runtimeMxBean.getSystemProperties());

    }
}
