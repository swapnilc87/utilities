#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
import csv
import requests
import math
import io
import os
import time
import datetime
import numpy as np
import findspark
findspark.init()
pd.options.display.max_rows = 400
pd.options.display.max_colwidth=200
pd.set_option('display.max_columns', None)
from pyspark.sql.functions import *
from pyspark.sql import *
from pyspark.sql.types import *
from pyspark.sql import functions as f
from pyspark.sql import types as t
import re
import datetime as dt
import matplotlib.pyplot as plt
import matplotlib
import seaborn as sns
import numpy as np
from collections import Counter
get_ipython().magic(u'matplotlib inline')


# In[2]:


import configparser
config = configparser.ConfigParser()
config.read(os.path.expanduser("~/.aws/credentials"))
access_id = config.get('default', "aws_access_key_id") 
access_key = config.get('default', "aws_secret_access_key")
print access_id,access_key


# In[3]:


spark = SparkSession.builder.config("spark.executor.memory","14000m").appName('query_filters_scoring_2').enableHiveSupport().getOrCreate()


# In[4]:


table_name='searchdb.query_filter_clusters_monthly'
s3_base_path="s3://myntra/searchteam/hive/"
current_date = '2019-01-11'
FILTERS_LIMIT=5
FILTER_VALUES_LIMIT=10
days_behind = 10
start_dt = dt.datetime.strptime(current_date,'%Y-%m-%d') - dt.timedelta(days=days_behind)
date_start = dt.datetime.strftime(start_dt,'%Y-%m-%d')
print date_start,current_date


# In[5]:


blacklisted_filters=['size_facet','Global Size','Categories','Coupons','Price','Age','Offers','Bundles','ComboId','keywords','Gender',"Global_attr_gender_string","global_attr_gender_string","Discount",'discount','Colour Family','Multipack Set']
global_filter_map = {"Brand":"brands_filter_facet","Color":"global_attr_base_colour_hex"}


# In[6]:


select_query="select distinct user_query from searchdb.atsa_gender_queries"

filtered_queries_df = spark.sql(select_query)


# In[7]:


# filtered_queries_df = query_impressions_df.filter(col('query_impressions')>1000).drop('query_impressions')


# In[8]:


filtered_queries_df.count()


# In[9]:


# f.broadcast(filtered_queries_df)
filtered_queries_df = filtered_queries_df.persist()
# filtered_queries_df = f.broadcast(filtered_queries_df)


# In[10]:


# filtered_queries_df.registerTempTable('filtered_queries_table')


# In[11]:


# filter_select_query="select parsed_url_struct['user_query_parsed'] as user_query, parsed_url_struct['applied_filters'] as applied_filters from searchdb.session_stitched_raw_daily where data_date>='{start_date}' and data_date<'{end_date}' and parsed_url_struct['user_query_parsed'] is not NULL and parsed_url_struct['user_query_parsed'] in (select user_query from filtered_queries_table)".format(start_date=date_start,end_date=current_date)
filter_select_query="select parsed_url_struct['user_query_parsed'] as user_query, parsed_url_struct['applied_filters'] as applied_filters from searchdb.session_stitched_raw_daily where data_date>='{start_date}' and data_date<'{end_date}' and parsed_url_struct['user_query_parsed'] is not NULL".format(start_date=date_start,end_date=current_date)

query_base_df = spark.sql(filter_select_query)

# query_base_df.count()


# In[12]:


query_filter_base_df = filtered_queries_df.join(query_base_df,'user_query',how='inner')
# query_filter_base_df.count()


# In[13]:


query_filters_df = query_filter_base_df.select(f.explode(col('applied_filters')).alias("filter_name","filter_value"),col('user_query')).withColumn("filter_name",f.regexp_replace(f.col('filter_name'),'_article_attr$',''))



# In[14]:


query_filters_df = query_filters_df.filter(col('filter_name').isin(blacklisted_filters)==False).withColumn('filter_name',when(f.lower(col('filter_name'))==col('filter_name'),f.initcap(col('filter_name'))).otherwise(col('filter_name'))).filter(col('filter_name').isin(blacklisted_filters)==False)



# In[15]:


query_filters_df


# In[16]:


query_filter_grouped_df = query_filters_df.groupBy(['user_query','filter_name']).agg(f.collect_list('filter_value').alias('values_list'),f.count('filter_value').alias('query_filter_impressions'))

# query_filter_grouped_df.count()
query_filter_grouped_df.persist()


# In[17]:


# query_filters_df.filter(col('user_query')=='jackets men').select(['user_query','filter_name']).drop_duplicates().show(100,False)


# In[18]:


def flatten_and_count(L,topn):
    if L is None:
        return None
    outputDict ={}
    if isinstance(L, list):
        for i in L:
            if i is not None:
                if isinstance(i, list):
                    for ele in i :
                        if ele in outputDict :
                            outputDict[ele] = outputDict[ele]+1
                        else :
                            outputDict[ele] = 1
                else:
                    if i in outputDict :
                        outputDict[i] = outputDict[i]+1
                    else :
                        outputDict[i] = 1
    if not outputDict:
        return None
    cnt = Counter(outputDict)
    outputDict.clear()
    for k,v in cnt.most_common(topn) :
        outputDict[k] = v
    return outputDict
flatten_and_count_str_udf = udf(flatten_and_count,MapType(StringType(),IntegerType()))
print flatten_and_count([[35,53,525],[525,6743],[],None,[64,63],None,[743,754,757]],2)
print flatten_and_count([],2)


# In[19]:


query_filter_value_df = query_filter_grouped_df.withColumn('filter_value_count',flatten_and_count_str_udf(col('values_list'),lit(FILTER_VALUES_LIMIT))).drop('values_list')
query_filter_value_df = query_filter_value_df.persist()


# In[20]:


query_filter_value_df.show(1000,False)


# In[21]:


# query_filter_value_df


# In[49]:


window = Window.partitionBy(col('user_query')).orderBy(col('query_filter_impressions').desc())

filtered_qfv_df = query_filter_value_df.select('*', rank().over(window).alias('rank')) .filter(col('rank') <= FILTERS_LIMIT) 
# filtered_qfv_df.count()


# In[23]:


filtered_qfv_df.show(10,False)


# In[48]:


def convert_filter_values(value_map,filter_name) :
    global_filter_map = {"Brand":"brands_filter_facet","Color":"global_attr_base_colour_hex_facet"}
    if filter_name.endswith('article_attr') :
        filter_name = filter_name.strip('article_attr')
    cnt = Counter(value_map)
    out=[]
    for k,v in cnt.most_common() :
        field_name = filter_name
        
        display_name = k
        if filter_name == 'Color' :
            display_name = k.split('_')[0]
            k = k.replace('_',':')
        display_name = display_name.title()
        if filter_name in global_filter_map:
            field_name = global_filter_map[filter_name]
        else :
            field_name = filter_name.strip().replace(' ','_')+'_article_attr'
        
        out.append([k,display_name,[[filter_name,field_name,k]]])
    
    return filter_name,filter_name,out

filters_schema = StructType([StructField("id",StringType()),StructField("field",StringType()),StructField("value",StringType())])
cluster_value_schema = StructType([StructField("id",StringType()),StructField("name",StringType()),StructField("filters",ArrayType(filters_schema))])
cluster_details_schema = StructType([StructField("id",StringType()),StructField("name",StringType()),StructField("clusterValues",ArrayType(cluster_value_schema))])

convert_filter_values_udf = udf(convert_filter_values,cluster_details_schema)

# print convert_filter_values({'v': 1},'f')
print convert_filter_values({'v1_aserr': 4,'v2': 3,'v3': 2,'v4': 1},'Color')



# In[50]:


filtered_qfv_df = filtered_qfv_df.withColumn('cluster_details_struct',(convert_filter_values_udf(col('filter_value_count'),col('filter_name')))).select('user_query','cluster_details_struct')




# In[51]:


filtered_qfv_df


# In[54]:


filtered_qfv_df.show(7,False)


# In[55]:


final_df = filtered_qfv_df.groupBy('user_query').agg(collect_list(col('cluster_details_struct')).alias('cluster_details_arr_struct')).withColumn('data_date', to_date(lit(current_date),'yyyy-MM-dd'))




# In[36]:


final_df.show(10,False)


# In[56]:


final_df.write.saveAsTable(name=table_name,format='orc',mode='append',partitionBy=['data_date'],path=s3_base_path+table_name)




# In[ ]:


# print convert_filter_values({'v1': 4,'v2': 3,'v3': 2,'v4': 1},'f')
# spark.sparkContext.getConf().getAll()


# In[ ]:


# jsn='{"id":"v","name":"v","filters":[{"id":"f","field":"f","value":"v"}]}'
# jsn='{"id":"f","name":"f","clusterValues":[{"id":"v1","filters":[{"id":"f","field":"f","value":"v1"}],"name":"v1"},{"id":"v2","filters":[{"id":"f","field":"f","value":"v2"}],"name":"v2"},{"id":"v3","filters":[{"id":"f","field":"f","value":"v3"}],"name":"v3"},{"id":"v4","filters":[{"id":"f","field":"f","value":"v4"}],"name":"v4"}]}'
# temp_df.withColumn('tmp',f.from_json(lit(jsn),cluster_details_schema)).show(10,False)


# In[ ]:


# spark.sql("select count(*) from searchdb.query_filter_clusters_monthly").show()


# In[ ]:


# [f, f, [[v1, v1, [[f, f, v1]]], [v2, v2, [[f, f, v2]]], [v3, v3, [[f, f, v3]]], [v4, v4, [[f, f, v4]]]]]
# [f, f, [[v1, v1, [[f, f, v1]]], [v2, v2, [[f, f, v2]]], [v3, v3, [[f, f, v3]]], [v4, v4, [[f, f, v4]]]]]
# f.regexp_replace?


# In[ ]:




