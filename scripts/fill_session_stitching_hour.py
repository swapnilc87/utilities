#!/usr/bin/env python
# coding: utf-8

# In[1]:

import sys
import pandas as pd
import csv
import requests
import math
import io
import os
import time
import datetime
import numpy as np
from pyspark.sql.functions import *
from pyspark.sql import *
from pyspark.sql.types import *
from pyspark.sql import functions as f
from pyspark.sql import types as t
import re
import datetime as dt
from pandas.io.json import json_normalize
import urlparse as parse


# In[2]:

#
# import configparser
# config = configparser.ConfigParser()
# config.read(os.path.expanduser("~/.aws/credentials"))
# access_id = config.get('default', "aws_access_key_id")
# access_key = config.get('default', "aws_secret_access_key")
# access_id


# In[3]:


spark = SparkSession.builder.appName('session_stitched_daily_backfill'+sys.argv[1]+'_'+sys.argv[2]).enableHiveSupport().getOrCreate()
# spark = SparkSession.builder.config("spark.executor.cores",4).config("spark.executor.memory","18000m").appName('session_stitched_daily_backfill').enableHiveSupport().getOrCreate()


# .config("spark.executor.memory","4200m")


# In[4]:


# spark.sparkContext.getConf().getAll()


# In[5]:


column_names=['session_auto_id','session_previous_screen','is_first_session','landing_screen','request_id','server_offset','session_id','session_start_time','customer_id','uidx','is_logged_in','prev_customer_id','customer_segments','event_meta_version','event_type','data_set_type','data_set_name','data_set_value','data_set_level','clicked_entity_type','clicked_entity_id','clicked_widget_type','clicked_widget_name','server_ts','client_ts','load_date','action_on_entity','prev_screen_name','prev_screen_entity_type','screen_name','card_type','search_text','suggest_usage','suggest_text','suggest_click_depth','widget_items_entity_name','widget_total_view','widget_items_total_view','sku_id','widget_name','widget_type','rating','order_id','scroll_position','screen_url']
retain_list = ['session_id','client_ts','event_type','suggest_usage','suggest_text','search_text','widget_name','widget_type','server_ts']
retain_column_list = ['session_id','client_ts','ab_tests','customer_id','device_id','event_type','items','prev_screen_url','screen_url','landing_screen','data_set_value','uidx','server_ts']
group_column_list = ['session_id','uidx','ab_tests','customer_id','device_id','common_url']
duplicate_drop_column_list = ['session_id','uidx','ab_tests','customer_id','device_id','common_url','shown_items','clicked_items','data_set_value']

entity_attributes_schema = StructType([StructField("v_position",DoubleType(),True), StructField("price",DoubleType(),True),StructField("isPersonalized",BooleanType(),True),StructField("h_position",DoubleType(),True),StructField("discounted_price",DoubleType(),True),StructField("list_item_position",DoubleType())])
item_struct = StructType([StructField("entity_id", StringType(),True),StructField("entity_type", StringType(),True),StructField("entity_optional_attributes",entity_attributes_schema,True),StructField("item_rank",DoubleType(),True)])
item_schema = ArrayType(item_struct)
applied_params_schema = StructType([StructField("applied_filters",MapType(StringType(),ArrayType(StringType()))),StructField("applied_range_filters",MapType(StringType(),ArrayType(StringType()))),StructField("applied_sort",StringType()),StructField("user_query_parsed",StringType()),StructField("extra_search_param",MapType(StringType(),ArrayType(StringType())))])

def flatten(L):
#     print "L=",L
    if L is None:
        return None
    outputList =[]
    if isinstance(L, list):
        for i in L:
            if i is not None:
                if isinstance(i, list):
                    outputList.extend(i)
                else:
                    outputList.append(i)
    if not outputList:
        return None
    return outputList
flatten_items_udf = udf(flatten,item_schema)
flatten_str_udf = udf(flatten,ArrayType(StringType()))
print flatten([[35,53],[525,6743],[],None,[64,63],None,[743,754,757]])
# print flatten([])

# sample_url = "https://api.myntra.com/saree-store?extra_search_param=IsAutoSuggestEntry:true::Id:2297-watches-for-women&f=Type_article_attr:kanjeevaram::Color:Grey%20Melange_9fa8ab,Gold_e5c74a,White_f2f2f2::Categories:Sarees&sort=price_asc&rf=Price:396.0_1394.0_396.0%20TO%201394.0,1924.0_2699.0_1924.0%20TO%202699.0,1395.0_1919.0_1395.0%20TO%201919.0"


def get_applied_params_struct(sample_url) :

    filters_applied = None
    range_filters_applied =None
    sort_val=None
    user_query=None
    extra_search_param=None
    if sample_url is None or '?' not in sample_url:
        return filters_applied,range_filters_applied,sort_val,user_query,extra_search_param

    params_dict  = dict(parse.parse_qsl(parse.urlparse(sample_url).query))
    if '?' in sample_url:
        part_url = sample_url.split('?')[0]
        if '/' in part_url :
            user_query = part_url.split('/')[-1]
            user_query = parse.unquote(user_query).strip(' ')
            user_query = s = re.sub('[^0-9a-zA-Z]+', ' ', user_query).lower()

    if'f' in params_dict :
        filters_applied = {}
        filter_entries = params_dict['f'].split('::')
        for entry in filter_entries :
            if ':' in entry :
                filter_key,filter_value = entry.split(':',1)
                filters_applied[filter_key]=filter_value.split(',')
    if'rf' in params_dict :
        range_filters_applied = {}
        filter_entries = params_dict['rf'].split('::')
        for entry in filter_entries :
#             print entry
            if ':' in entry :
                filter_key,filter_value = entry.split(':',1)
                if '_' in filter_value :
                    try :
                        range_filters_applied[filter_key]=map(lambda x : '_'.join(x.split('_')[:2]),filter_value.split(','))
                    except :
                        None

    if 'sort' in params_dict:
        sort_val = params_dict['sort']
    if 'extra_search_param' in params_dict :
        extra_search_param = {}
        filter_entries = params_dict['extra_search_param'].split('::')
        for entry in filter_entries :
            if ':' in entry :
                filter_key,filter_value = entry.split(':',1)
                extra_search_param[filter_key]=filter_value.split(',')

    return filters_applied,range_filters_applied,sort_val,user_query,extra_search_param

get_applied_params_struct_udf = udf(get_applied_params_struct,applied_params_schema)

# applied_params_struct_type = StructType([StructField('applied_filters',MapType(StringType(),ArrayType(StringType())))])


# get_applied_params_struct(sample_url)


# In[9]:

table_name='searchdb.session_stitched_raw_daily'
s3_base_path="s3://myntra/searchteam/hive/"
# current_date = '2018-11-30'
current_date = sys.argv[1]
total_days = 1
maxx = 24

now_dt = dt.datetime.strptime(current_date,'%Y-%m-%d')
year = now_dt.year
month = now_dt.month
day = now_dt.day
if day< 10 :
    day ='0'+str(day)
if month < 10 :
    month = '0'+str(month)
hr = int(sys.argv[2])
if hr < 10 :
     hr = '0'+str(hr)

print year,month,day,hr
input_path = 's3://madlytics/flattenedDataOrc/data=eventsTable/y={}/m={}/d={}/h={}/{}/'.format(year,month,day,hr,'min=*')
print input_path,dt.datetime.now()
date_today = dt.datetime.strftime(now_dt,'%Y-%m-%d')
#         print date_today

events_df = spark.read.orc(input_path).select(retain_column_list)
events_df = events_df.where(col('event_type').isin(['product_click_list_page','Product list loaded','addToCart']))

#         events_df = events_df.withColumn("search_query_text",when(events_df.suggest_usage.eqNullSafe("true"),f.lower(events_df.suggest_text)).otherwise(f.lower(events_df.search_text))).drop("suggest_usage","suggest_text","search_text").withColumn('search_query_text', when(col('search_query_text') == '', None).otherwise(col('search_query_text')))
events_df = events_df.withColumn('session_id', when(col('session_id') == '', None).otherwise(col('session_id'))).dropna(how='any',subset=['session_id'])
events_df = events_df.withColumn('common_url',when(col('event_type')=='addToCart',col('prev_screen_url')).otherwise(col('screen_url')))
events_df = events_df.withColumn('common_url',when(col('common_url')=='',None).otherwise(col('common_url')))
events_df = events_df.withColumn('uidx',when(col('uidx')=='',None).otherwise(col('uidx')))
events_df = events_df.withColumn('customer_id',when(col('customer_id')=='',None).otherwise(col('customer_id')))

# events_df = events_df.withColumn('items_struct',f.from_json(col('items'),schema=item_schema)).withColumn('parsed_screen_url',get_applied_params_struct_udf(col('screen_url')))
events_df = events_df.withColumn('shown_items',when(col('event_type')=='Product list loaded',f.from_json(col('items'),schema=item_schema)).otherwise(None))
events_df = events_df.withColumn('clicked_items',when(col('event_type')=='product_click_list_page',f.from_json(col('items'),schema=item_schema)).otherwise(None))
events_df = events_df.drop(col('items')).drop(col('screen_url')).drop(col('prev_screen_url')).sort(["session_id", "client_ts"])
events_df.persist()
grouped_df = events_df.dropDuplicates(duplicate_drop_column_list).groupby(group_column_list).agg(collect_list(col('shown_items')).alias('all_shown_items'),collect_list(col('clicked_items')).alias('all_clicked_items'),collect_set(col('landing_screen')).alias('all_landing_screens'),collect_list(col('data_set_value')).alias('atc_items'),collect_list(col('event_type')).alias('all_event_types'),collect_list(col('client_ts')).alias('all_client_ts'),collect_list(col('server_ts')).alias('all_server_ts'))


final_filter_df = grouped_df.withColumn("shown_items",flatten_items_udf(col('all_shown_items'))).withColumn("clicked_items",flatten_items_udf(col('all_clicked_items'))).withColumn("addedToCart_items",flatten_str_udf(col('atc_items'))).drop("atc_items").drop("all_clicked_items").drop("all_shown_items").dropna(how='any',subset=['session_id','common_url']).withColumn('parsed_url_struct',get_applied_params_struct_udf('common_url')).withColumn('data_date', to_date(lit(now_dt),'yyyy-MM-dd')).withColumn('hr', lit(hr)).withColumn('data_date_col', to_date(lit(now_dt),'yyyy-MM-dd'))
final_filter_df.persist()
events_df.unpersist(True)
drop_query = "ALTER TABLE {table_name} DROP IF EXISTS PARTITION (data_date='{val}' , hr='{hr}')".format(hr=hr,val=date_today,table_name=table_name)
print drop_query
spark.sql(drop_query)

# write data into date's partition
#         final_filter_df = final_filter_df.coalesce(10)
#         final_filter_df.coalesce(10).write.format("orc").save(s3_base_path+'abc')
final_filter_df.coalesce(10).write.saveAsTable(name=table_name,format='orc',mode='append',partitionBy=['data_date','hr'],path=s3_base_path+table_name)
print "finished writing session data for date ",year,month,day,hr
print "time now:",dt.datetime.now()
final_filter_df.unpersist()
spark.catalog.clearCache()
print "finiished clearing cache",dt.datetime.now()

