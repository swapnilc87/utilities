CREATE EXTERNAL TABLE `query_filter_clusters_monthly`(
  `user_query` string,
  `cluster_details_arr_struct` array<struct<id:string,name:string,clusterValues:array<struct<id:string,name:string,filters:array<struct<id:string,field:string,value:string>>>>>>)
PARTITIONED BY (
  `data_date` date)
ROW FORMAT SERDE
  'org.apache.hadoop.hive.ql.io.orc.OrcSerde'
WITH SERDEPROPERTIES (
  'path'='s3://myntra/searchteam/hive/searchdb.query_filter_clusters_monthly')
STORED AS INPUTFORMAT
  'org.apache.hadoop.hive.ql.io.orc.OrcInputFormat'
OUTPUTFORMAT
  'org.apache.hadoop.hive.ql.io.orc.OrcOutputFormat'
LOCATION
  's3://myntra/searchteam/hive/searchdb.query_filter_clusters_monthly'
TBLPROPERTIES (
  'spark.sql.create.version'='2.3.1',
  'spark.sql.partitionProvider'='catalog',
  'spark.sql.sources.provider'='orc',
  'spark.sql.sources.schema.numPartCols'='1',
  'spark.sql.sources.schema.numParts'='1',
  'spark.sql.sources.schema.part.0'='{"type":"struct","fields":[{"name":"user_query","type":"string","nullable":true,"metadata":{}},{"name":"cluster_details_arr_struct","type":{"type":"array","elementType":{"type":"struct","fields":[{"name":"id","type":"string","nullable":true,"metadata":{}},{"name":"name","type":"string","nullable":true,"metadata":{}},{"name":"clusterValues","type":{"type":"array","elementType":{"type":"struct","fields":[{"name":"id","type":"string","nullable":true,"metadata":{}},{"name":"name","type":"string","nullable":true,"metadata":{}},{"name":"filters","type":{"type":"array","elementType":{"type":"struct","fields":[{"name":"id","type":"string","nullable":true,"metadata":{}},{"name":"field","type":"string","nullable":true,"metadata":{}},{"name":"value","type":"string","nullable":true,"metadata":{}}]},"containsNull":true},"nullable":true,"metadata":{}}]},"containsNull":true},"nullable":true,"metadata":{}}]},"containsNull":true},"nullable":true,"metadata":{}},{"name":"data_date","type":"date","nullable":true,"metadata":{}}]}',
  'spark.sql.sources.schema.partCol.0'='data_date',
  'transient_lastDdlTime'='1557867462')