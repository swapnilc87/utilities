CREATE EXTERNAL TABLE `session_stitched_raw_daily_new`(
  `session_id` string,
  `uidx` string,
  `ab_tests` string,
  `customer_id` string,
  `device_id` string,
  `common_url` string,
  `all_landing_screens` array<string>,
  `all_event_types` array<string>,
  `all_client_ts` array<string>,
  `all_server_ts` array<string>,
  `shown_items` array<struct<entity_id:string,entity_type:string,entity_optional_attributes:struct<v_position:double,price:double,isPersonalized:boolean,h_position:double,discounted_price:double,adId:string,product_ids_shown:string,list_item_position:double>,item_rank:double>>,
  `clicked_items` array<struct<entity_id:string,entity_type:string,entity_optional_attributes:struct<v_position:double,price:double,isPersonalized:boolean,h_position:double,discounted_price:double,adId:string,product_ids_shown:string,list_item_position:double>,item_rank:double>>,
  `addedtocart_items` array<string>,
  `parsed_url_struct` struct<applied_filters:map<string,array<string>>,applied_range_filters:map<string,array<string>>,applied_sort:string,user_query_parsed:string,extra_search_param:map<string,array<string>>>,
  `data_date_col` date)
PARTITIONED BY (
  `data_date` date,
  `hr` string)
ROW FORMAT SERDE
  'org.apache.hadoop.hive.ql.io.orc.OrcSerde'
WITH SERDEPROPERTIES (
  'path'='s3://myntra/searchteam/hive/searchdb.session_stitched_raw_daily_new')
STORED AS INPUTFORMAT
  'org.apache.hadoop.hive.ql.io.orc.OrcInputFormat'
OUTPUTFORMAT
  'org.apache.hadoop.hive.ql.io.orc.OrcOutputFormat'
LOCATION
  's3://myntra/searchteam/hive/searchdb.session_stitched_raw_daily_new'
TBLPROPERTIES (
  'spark.sql.create.version'='2.3.1',
  'spark.sql.partitionProvider'='catalog',
  'spark.sql.sources.provider'='orc',
  'spark.sql.sources.schema.numPartCols'='2',
  'spark.sql.sources.schema.numParts'='2',
  'spark.sql.sources.schema.part.0'='{"type":"struct","fields":[{"name":"session_id","type":"string","nullable":true,"metadata":{}},{"name":"uidx","type":"string","nullable":true,"metadata":{}},{"name":"ab_tests","type":"string","nullable":true,"metadata":{}},{"name":"customer_id","type":"string","nullable":true,"metadata":{}},{"name":"device_id","type":"string","nullable":true,"metadata":{}},{"name":"common_url","type":"string","nullable":true,"metadata":{}},{"name":"all_landing_screens","type":{"type":"array","elementType":"string","containsNull":true},"nullable":true,"metadata":{}},{"name":"all_event_types","type":{"type":"array","elementType":"string","containsNull":true},"nullable":true,"metadata":{}},{"name":"all_client_ts","type":{"type":"array","elementType":"string","containsNull":true},"nullable":true,"metadata":{}},{"name":"all_server_ts","type":{"type":"array","elementType":"string","containsNull":true},"nullable":true,"metadata":{}},{"name":"shown_items","type":{"type":"array","elementType":{"type":"struct","fields":[{"name":"entity_id","type":"string","nullable":true,"metadata":{}},{"name":"entity_type","type":"string","nullable":true,"metadata":{}},{"name":"entity_optional_attributes","type":{"type":"struct","fields":[{"name":"v_position","type":"double","nullable":true,"metadata":{}},{"name":"price","type":"double","nullable":true,"metadata":{}},{"name":"isPersonalized","type":"boolean","nullable":true,"metadata":{}},{"name":"h_position","type":"double","nullable":true,"metadata":{}},{"name":"discounted_price","type":"double","nullable":true,"metadata":{}},{"name":"adId","type":"string","nullable":true,"metadata":{}},{"name":"product_ids_shown","type":"string","nullable":true,"metadata":{}},{"name":"list_item_position","type":"double","nullable":true,"metadata":{}}]},"nullable":true,"metadata":{}},{"name":"item_rank","type":"double","nullable":true,"metadata":{}}]},"containsNull":true},"nullable":true,"metadata":{}},{"name":"clicked_items","type":{"type":"array","elementType":{"type":"struct","fields":[{"name":"entity_id","type":"string","nullable":true,"metadata":{}},{"name":"entity_type","type":"string","nullable":true,"metadata":{}},{"name":"entity_optional_attributes","type":{"type":"struct","fields":[{"name":"v_position","type":"double","nullable":true,"metadata":{}},{"name":"price","type":"double","nullable":true,"metadata":{}},{"name":"isPersonalized","type":"boolean","nullable":true,"metadata":{}},{"name":"h_position","type":"double","nullable":true,"metadata":{}},{"name":"discounted_price","type":"double","nullable":true,"metadata":{}},{"name":"adId","type":"string","nullable":true,"metadata":{}},{"name":"product_ids_shown","type":"string","nullable":true,"metadata":{}},{"name":"list_item_position","type":"double","nullable":true,"metadata":{}}]},"nullable":true,"metadata":{}},{"name":"item_rank","type":"double","nullable":true,"metadata":{}}]},"containsNull":true},"nullable":true,"metadata":{}},{"name":"addedToCart_items","type":{"type":"array","elementType":"string","containsNull":true},"nullable":true,"metadata":{}},{"name":"parsed_url_struct","type":{"type":"struct","fields":[{"name":"applied_filters","type":{"type":"map","keyType":"string","valueType":{"type":"array","elementType":"string","containsNull":true},"valueContainsNull":true},"nullable":true,"metadata":{}},{"name":"applied_range_filters","type":{"type":"map","keyType":"string","valueType":{"type":"array","elementType":"string","containsNull":true},"valueContainsNull":true},"nullable":true,"metadata":{}},{"name":"applied_sort","type":"string","nullable":true,"metadata":{}},{"name":"user_query_parsed","type":"string","nullable":true,"metadata":{}},{"name":"extra_search_param","type":{"type":"map","keyType":"string","valueType":{"type":"array","elementType":"string","containsNull":true},"valueContainsNull":true},"nullable":true,"metadata":{}}]},"nullable":true,"metadata":{}},{"name":"data_date_col","type":"date","nullable":true,"metadata":{}},{"name":"data_date","type":"date",',
  'spark.sql.sources.schema.part.1'='"nullable":true,"metadata":{}},{"name":"hr","type":"string","nullable":true,"metadata":{}}]}',
  'spark.sql.sources.schema.partCol.0'='data_date',
  'spark.sql.sources.schema.partCol.1'='hr',
  'transient_lastDdlTime'='1558266933')