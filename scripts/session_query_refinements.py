#!/usr/bin/env python
# coding: utf-8

# In[1]:

import sys
from pyspark.sql.functions import *
from pyspark.sql import *
from pyspark.sql.types import *
from pyspark.sql import functions as f
import datetime as dt

spark = SparkSession.builder.appName('session_query_refinements_'+sys.argv[1]+'_'+sys.argv[2]).enableHiveSupport().getOrCreate()
# spark = SparkSession.builder.config("spark.executor.cores",4).config("spark.executor.memory","18000m").appName('session_stitched_daily_backfill').enableHiveSupport().getOrCreate()

table_name='searchdb.session_query_refinements'
s3_base_path="s3://myntra/searchteam/hive/"
# current_date = '2018-11-30'
current_date = sys.argv[1]
total_days = 1
maxx = 24

now_dt = dt.datetime.strptime(current_date,'%Y-%m-%d')
year = now_dt.year
month = now_dt.month
day = now_dt.day
if day< 10 :
    day ='0'+str(day)
if month < 10 :
    month = '0'+str(month)
hr = int(sys.argv[2])
if hr < 10 :
    hr = '0'+str(hr)

print year,month,day,hr

session_window = Window.partitionBy("session_id").orderBy("client_ts").rowsBetween(Window.currentRow, 3)


date_today = dt.datetime.strftime(now_dt,'%Y-%m-%d')
#         print date_today
SELECT_QUERY="select parsed_url_struct['user_query_parsed'] as user_query,session_id,all_client_ts[0] as client_ts from searchdb.session_stitched_raw_daily_new where data_date='{current_date}' and hr='{hr}' and parsed_url_struct['user_query_parsed'] is not null".format(current_date=date_today,hr=hr)
query_df = spark.sql(SELECT_QUERY)


query_df = query_df.drop_duplicates(['user_query','session_id'])
reformulations_df = query_df.withColumn('reformulations',f.collect_list(col('user_query')).over(session_window))
exploded_df = reformulations_df.select('user_query','session_id',f.posexplode(col('reformulations')).alias('posn','candidate_refinement')).drop('reformulations')
exploded_df = exploded_df.where(col('user_query')!=col('candidate_refinement'))
aggregated_df = exploded_df.groupBy('user_query','candidate_refinement').agg(f.countDistinct('session_id').alias('impressions'),f.avg(lit(1)-((col('posn')-1)/3)).alias('position_score')).sort(col('position_score').desc(),col('impressions').desc())

final_filter_df = aggregated_df.withColumn('data_date', to_date(lit(now_dt),'yyyy-MM-dd')).withColumn('hr', lit(hr)).withColumn('data_date_col', to_date(lit(now_dt),'yyyy-MM-dd'))
drop_query = "ALTER TABLE {table_name} DROP IF EXISTS PARTITION (data_date='{val}' , hr='{hr}')".format(hr=hr,val=date_today,table_name=table_name)
print drop_query
spark.sql(drop_query)

final_filter_df.coalesce(1).write.saveAsTable(name=table_name,format='orc',mode='append',partitionBy=['data_date','hr'],path=s3_base_path+table_name)
print "finished writing session refinement data for date ",year,month,day,hr
print "time now:",dt.datetime.now()
spark.catalog.clearCache()
print "finished clearing cache",dt.datetime.now()

