#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
import csv
import requests
import math
import io
import os
import time
import datetime
import numpy as np
from pyspark.sql.functions import *
from pyspark.sql import *
from pyspark.sql.types import *
from pyspark.sql import functions as f
from pyspark.sql import types as t
import re
import datetime as dt
import numpy as np
from collections import Counter
import sys



# In[3]:


spark = SparkSession.builder.appName('query_filters_scoring').enableHiveSupport().getOrCreate()


# In[4]:


table_name='searchdb.query_filter_clusters_monthly'
s3_base_path="s3://myntra/searchteam/hive/"
current_date = sys.argv[1]
FILTERS_LIMIT=5
FILTER_VALUES_LIMIT=10
days_behind = 10
start_dt = dt.datetime.strptime(current_date,'%Y-%m-%d') - dt.timedelta(days=days_behind)
date_start = dt.datetime.strftime(start_dt,'%Y-%m-%d')


# In[5]:


blacklisted_filters=['size_facet','Global Size','Categories','Coupons','Price','Age','Offers','Bundles','ComboId','keywords','Gender','Brand','Color']
global_filter_map = {"Brand":"brands_filter_facet","Color":"global_attr_base_colour_hex_facet"}


# In[6]:


select_query="select parsed_url_struct['user_query_parsed'] as user_query, count(*) as query_impressions from searchdb.session_stitched_raw_daily where data_date>='{start_date}' and data_date<'{end_date}' and parsed_url_struct['user_query_parsed'] is not NULL and parsed_url_struct['extra_search_param']['IsAutoSuggestEntry'][0]=='true' group by parsed_url_struct['user_query_parsed'] order by query_impressions desc".format(start_date=date_start,end_date=current_date)

query_impressions_df = spark.sql(select_query)


# In[7]:


filtered_queries_df = query_impressions_df.filter(col('query_impressions')>1000).drop('query_impressions')


# In[8]:


# f.broadcast(filtered_queries_df)
filtered_queries_df = filtered_queries_df.persist()
# filtered_queries_df = f.broadcast(filtered_queries_df)


# In[9]:


# filtered_queries_df.registerTempTable('filtered_queries_table')


# In[10]:


# filter_select_query="select parsed_url_struct['user_query_parsed'] as user_query, parsed_url_struct['applied_filters'] as applied_filters from searchdb.session_stitched_raw_daily where data_date>='{start_date}' and data_date<'{end_date}' and parsed_url_struct['user_query_parsed'] is not NULL and parsed_url_struct['user_query_parsed'] in (select user_query from filtered_queries_table)".format(start_date=date_start,end_date=current_date)
filter_select_query="select parsed_url_struct['user_query_parsed'] as user_query, parsed_url_struct['applied_filters'] as applied_filters from searchdb.session_stitched_raw_daily where data_date>='{start_date}' and data_date<'{end_date}' and parsed_url_struct['user_query_parsed'] is not NULL".format(start_date=date_start,end_date=current_date)

query_base_df = spark.sql(filter_select_query)

query_filter_base_df = filtered_queries_df.join(query_base_df,'user_query',how='inner')
query_filter_base_df


# In[11]:


query_filters_df = query_filter_base_df.select(f.explode(col('applied_filters')).alias("filter_name","filter_value"),col('user_query'))


# In[12]:


query_filters_df = query_filters_df.filter(col('filter_name').isin(blacklisted_filters)==False).withColumn('filter_name',when(f.lower(col('filter_name'))==col('filter_name'),f.initcap(col('filter_name'))).otherwise(col('filter_name'))).filter(col('filter_name').isin(blacklisted_filters)==False)


# In[13]:


query_filter_grouped_df = query_filters_df.groupBy(['user_query','filter_name']).agg(f.collect_list('filter_value').alias('values_list'),f.count('filter_value').alias('query_filter_impressions'))
query_filter_grouped_df.persist()
# query_filter_grouped_df.show(30)


# In[15]:


def flatten_and_count(L,topn):
    if L is None:
        return None
    outputDict ={}
    if isinstance(L, list):
        for i in L:
            if i is not None:
                if isinstance(i, list):
                    for ele in i :
                        if ele in outputDict :
                            outputDict[ele] = outputDict[ele]+1
                        else :
                            outputDict[ele] = 1
                else:
                    if i in outputDict :
                        outputDict[i] = outputDict[i]+1
                    else :
                        outputDict[i] = 1
    if not outputDict:
        return None
    cnt = Counter(outputDict)
    outputDict.clear()
    for k,v in cnt.most_common(topn) :
        outputDict[k] = v
    return outputDict
flatten_and_count_str_udf = udf(flatten_and_count,MapType(StringType(),IntegerType()))
print flatten_and_count([[35,53,525],[525,6743],[],None,[64,63],None,[743,754,757]],2)
print flatten_and_count([],2)


# In[16]:


query_filter_value_df = query_filter_grouped_df.withColumn('filter_value_count',flatten_and_count_str_udf(col('values_list'),lit(FILTER_VALUES_LIMIT))).drop('values_list')
query_filter_value_df.persist()

# In[19]:


window = Window.partitionBy(col('user_query')).orderBy(col('query_filter_impressions').desc())

filtered_qfv_df = query_filter_value_df.select('*', rank().over(window).alias('rank')) .filter(col('rank') <= FILTERS_LIMIT)


# In[20]:


def convert_filter_values(value_map,filter_name) :
    cnt = Counter(value_map)
    out=[]
    for k,v in cnt.most_common() :
        out.append([k,k,[[filter_name,filter_name,k]]])

    return filter_name,filter_name,out

filters_schema = StructType([StructField("id",StringType()),StructField("field",StringType()),StructField("value",StringType())])
cluster_value_schema = StructType([StructField("id",StringType()),StructField("name",StringType()),StructField("filters",ArrayType(filters_schema))])
cluster_details_schema = StructType([StructField("id",StringType()),StructField("name",StringType()),StructField("clusterValues",ArrayType(cluster_value_schema))])

convert_filter_values_udf = udf(convert_filter_values,cluster_details_schema)

print convert_filter_values({'v1': 4,'v2': 3,'v3': 2,'v4': 1},'f')



# In[21]:


filtered_qfv_df = filtered_qfv_df.withColumn('cluster_details_struct',convert_filter_values_udf(col('filter_value_count'),col('filter_name'))).select('user_query','cluster_details_struct')




# In[22]:


final_df = filtered_qfv_df.groupBy('user_query').agg(collect_list(col('cluster_details_struct')).alias('cluster_details_arr_struct')).withColumn('data_date', to_date(lit(current_date),'yyyy-MM-dd'))


# In[ ]:

drop_query = "ALTER TABLE {table_name} DROP IF EXISTS PARTITION (data_date='{val}')".format(val=current_date,table_name=table_name)
print drop_query
# spark.sql(drop_query)
final_df.coalesce(50).write.saveAsTable(name=table_name,format='orc',mode='append',partitionBy=['data_date'],path=s3_base_path+table_name)
