import pandas as pd
import csv
import requests
import math
import io
import os
import time
import datetime
import numpy as np
import findspark
findspark.init()
pd.options.display.max_rows = 400
pd.options.display.max_colwidth=200
pd.set_option('display.max_columns', None)
from pyspark.sql.functions import *
from pyspark.sql import *
from pyspark.sql.types import *
from pyspark.sql import functions as f
from pyspark.sql import types as t
import re
import datetime as dt
import numpy as np
from collections import Counter
%matplotlib inline
import json

spark = SparkSession.builder.appName('query_crawl_and_dump').enableHiveSupport().getOrCreate()

base_queries_table_name='searchdb.filtered_cluster_queries_base'
FILTERS_LIMIT=4
gender_at_queries_df = spark.sql("select * from searchdb.filtered_cluster_queries_base")
gender_at_queries_df

queries=[str(rw.user_query) for rw in gender_at_queries_df.select('user_query').collect()]

current_date='2019-04-26'
table_name='searchdb.query_filters_valid_daily'
s3_base_path="s3://myntra/searchteam/hive/"

now_dt = dt.datetime.strptime(current_date,'%Y-%m-%d')

def crawl_facet(query_str,FILTERS_LIMIT) :
    base_url='http://searchinternal-v2.myntra.com/search-service/searchservice/search/getresults/'
    headers ={'accept': 'application/json' ,'content-type': 'application/json','x-mynt-ctx':'storeid=2297'}
    requestBody = {"query":query_str,"start":0,"rows":48,"storefrontId":"test16","appliedParams":{"filters":[],"rangeFilters":[],"sort":"","geoFilters":[]},"returnDocs":False,"isFacet":True,"behaviouralMetaData":{"inlineFilterStatus":{"isFilterUnselected":False,"inlineFilterOffset":0,"inlineFilterCount":0}},"userQuery":False}
    response = requests.post(base_url,headers=headers,data=json.dumps(requestBody))
    resp = json.loads(response.text)
    secondary_filters = resp['response']['filters']['secondaryFilters']
    correctedQuery = query_str
    if 'correctedQuery' in resp:
        correctedQuery = resp['correctedQuery']
    valid_filters=[]
    no_of_filts=0
    for atsa_filter in secondary_filters:
        filter_name = atsa_filter['id']
        filter_values=[]
        for entry in atsa_filter['filterValues'] :
            if entry['count'] >30 :
                filter_values.append(entry['id'])
        if len(filter_values)>3:
            no_of_filts+=1
            for val in filter_values:
                tup = (query_str,correctedQuery,filter_name,val,val.lower())
                valid_filters.append(tup)
    if no_of_filts < FILTERS_LIMIT:
        return []
    return valid_filters


lst = crawl_facet('shirts for men',FILTERS_LIMIT)
query_valid_filters_df = spark.createDataFrame(lst,['user_query','correctedQuery' ,'filter_name', 'filter_value', 'filter_value_lower'])
for query in queries:
    print query
    lst = crawl_facet(query,FILTERS_LIMIT)
    if len(lst) ==0:
        continue
    df = spark.createDataFrame(lst,['user_query', 'correctedQuery','filter_name', 'filter_value', 'filter_value_lower'])
    query_valid_filters_df = query_valid_filters_df.union(df)


query_valid_filters_df = query_valid_filters_df.drop_duplicates().filter(col('filter_value')!='NA').withColumn('data_date', to_date(lit(now_dt),'yyyy-MM-dd'))



drop_query = "ALTER TABLE {table_name} DROP IF EXISTS PARTITION (data_date='{val}')".format(val=current_date,table_name=table_name)
print drop_query
spark.sql(drop_query)


query_valid_filters_df.coalesce(10).write.saveAsTable(name=table_name,format='orc',mode='append',partitionBy=['data_date'],path=s3_base_path+table_name)

