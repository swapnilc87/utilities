import pandas as pd
import csv
import requests
import math
import io
import os
import time
import datetime
import numpy as np
import findspark
findspark.init()
pd.options.display.max_rows = 400
pd.options.display.max_colwidth=200
pd.set_option('display.max_columns', None)
from pyspark.sql.functions import *
from pyspark.sql import *
from pyspark.sql.types import *
from pyspark.sql import functions as f
from pyspark.sql import types as t
import re
import datetime as dt
import matplotlib.pyplot as plt
import matplotlib
import seaborn as sns
import numpy as np
from collections import Counter
%matplotlib inline

spark = SparkSession.builder.config("spark.executor.memory","14000m").appName('query_filters_scoring_2').enableHiveSupport().getOrCreate()


table_name='searchdb.query_filter_clusters_monthly'
s3_base_path="s3://myntra/searchteam/hive/"
current_date = '2019-04-30'
FILTERS_LIMIT=4
FILTER_VALUES_LIMIT=25
days_behind = 30
start_dt = dt.datetime.strptime(current_date,'%Y-%m-%d') - dt.timedelta(days=days_behind)
date_start = dt.datetime.strftime(start_dt,'%Y-%m-%d')
print date_start,current_date

blacklisted_filters=['size_facet','Global Size','Categories','Coupons','Price','Age','Offers','Bundles','ComboId','keywords','Gender',"Global_attr_gender_string","global_attr_gender_string","Discount",'discount','Colour Family','Multipack Set','Color','Brand','brands_filter_facet','global_attr_base_colour_hex','global_attr_brand']
global_filter_map = {"Brand":"brands_filter_facet","Color":"global_attr_base_colour_hex"}


select_query="select distinct user_query from searchdb.filtered_cluster_queries_base"

filtered_queries_df = spark.sql(select_query)


filtered_queries_df.count()

# f.broadcast(filtered_queries_df)
filtered_queries_df = filtered_queries_df.persist()
# filtered_queries_df = f.broadcast(filtered_queries_df)

# filter_select_query="select parsed_url_struct['user_query_parsed'] as user_query, parsed_url_struct['applied_filters'] as applied_filters from searchdb.session_stitched_raw_daily where data_date>='{start_date}' and data_date<'{end_date}' and parsed_url_struct['user_query_parsed'] is not NULL and parsed_url_struct['user_query_parsed'] in (select user_query from filtered_queries_table)".format(start_date=date_start,end_date=current_date)
filter_select_query="select parsed_url_struct['user_query_parsed'] as user_query, parsed_url_struct['applied_filters'] as applied_filters from searchdb.session_stitched_raw_daily where data_date>='{start_date}' and data_date<'{end_date}' and parsed_url_struct['user_query_parsed'] is not NULL".format(start_date=date_start,end_date=current_date)

query_base_df = spark.sql(filter_select_query)

# query_base_df.count()

query_filter_base_df = filtered_queries_df.join(query_base_df,'user_query',how='inner')
# query_filter_base_df.count()

query_filters_df = query_filter_base_df.select(f.explode(col('applied_filters')).alias("filter_name","filter_value"),col('user_query')).withColumn("filter_name",f.regexp_replace(f.col('filter_name'),'_article_attr$',''))


query_filters_df = query_filters_df.filter(col('filter_name').isin(blacklisted_filters)==False).withColumn('filter_name',when(f.lower(col('filter_name'))==col('filter_name'),f.initcap(col('filter_name'))).otherwise(col('filter_name'))).filter(col('filter_name').isin(blacklisted_filters)==False)


query_filter_grouped_df = query_filters_df.groupBy(['user_query','filter_name']).agg(f.collect_list('filter_value').alias('values_list'),f.count('filter_value').alias('query_filter_impressions'))

# query_filter_grouped_df.count()
query_filter_grouped_df.persist()


max_dt_df = spark.sql("select max(data_date) as max_date from searchdb.query_filters_valid_daily")
max_date = [str(w.max_date) for w in max_dt_df.select('max_date').collect()][0]

query_valid_filters_df=spark.sql("select * from searchdb.query_filters_valid_daily where data_date='{max_date}'".format(max_date=max_date))


query_valid_filters_grouped_df = query_valid_filters_df.groupby(col('user_query'),col('filter_name')).agg(collect_set('filter_value').alias('valid_filter_value_list'))
query_valid_filters_grouped_df


filtered_query_filter_grouped_df = query_valid_filters_grouped_df.join(query_filter_grouped_df,how='inner',on=['user_query','filter_name'])
filtered_query_filter_grouped_df

def flatten_and_count(L,valid_values,topn):
    if L is None:
        return None
    outputDict ={}
    if isinstance(L, list):
        for i in L:
            if i is not None:
                if isinstance(i, list):
                    for ele in i :
                        if ele not in valid_values:
                            continue;

                        if ele in outputDict :
                            outputDict[ele] = outputDict[ele]+1
                        else :
                            outputDict[ele] = 1
                else:
                    if i not in valid_values:
                        continue;
                    if i in outputDict :
                        outputDict[i] = outputDict[i]+1
                    else :
                        outputDict[i] = 1
    for val in valid_values:
        if val not in outputDict:
            outputDict[val] = 1
    if not outputDict:
        return None
    cnt = Counter(outputDict)
    outputDict.clear()
    for k,v in cnt.most_common(topn) :
        outputDict[k] = v
    return outputDict
flatten_and_count_str_udf = udf(flatten_and_count,MapType(StringType(),IntegerType()))
print flatten_and_count([[35,53,525],[525,6743],[],None,[64,63],None,[743,754,757]],[525,63,99],20)
print flatten_and_count([],[],2)


query_filter_value_df = filtered_query_filter_grouped_df.withColumn('filter_value_count',flatten_and_count_str_udf(col('values_list'),col('valid_filter_value_list'),lit(FILTER_VALUES_LIMIT))).drop('values_list','valid_filter_value_list')
query_filter_value_df = query_filter_value_df.persist()


window = Window.partitionBy(col('user_query')).orderBy(col('query_filter_impressions').desc())

filtered_qfv_df = query_filter_value_df.select('*', rank().over(window).alias('rank')) .filter(col('rank') <= FILTERS_LIMIT)
# filtered_qfv_df.count()
filtered_qfv_df

def convert_filter_values(value_map,filter_name) :
    global_filter_map = {"Brand":"brands_filter_facet","Color":"global_attr_base_colour_hex_facet"}
    if filter_name.endswith('article_attr') :
        filter_name = filter_name.replace('_article_attr','')
        filter_name = filter_name.replace('_',' ')

    if filter_name.endswith('atsa_facet_') :
        filter_name = filter_name.replace('atsa_facet_','')
        filter_name = filter_name.replace('_',' ')

    cnt = Counter(value_map)
    out=[]
    for k,v in cnt.most_common() :
        field_name = filter_name

        display_name = k
        if filter_name == 'Color' :
            display_name = k.split('_')[0]
            k = k.replace('_',':')
        display_name = display_name.title()
        if filter_name in global_filter_map:
            field_name = global_filter_map[filter_name]
        else :
            #             field_name = filter_name.strip().replace(' ','_')+'_article_attr'
            field_name = 'atsa_facet_'+filter_name.strip().replace(' ','_')
        # remove lower from below line. This is temporary fix
        #         out.append([k,display_name,[[filter_name,field_name,k.lower()]]])
        out.append([k,display_name,[[filter_name,field_name,k]]])

    return filter_name,filter_name,out

filters_schema = StructType([StructField("id",StringType()),StructField("field",StringType()),StructField("value",StringType())])
cluster_value_schema = StructType([StructField("id",StringType()),StructField("name",StringType()),StructField("filters",ArrayType(filters_schema))])
cluster_details_schema = StructType([StructField("id",StringType()),StructField("name",StringType()),StructField("clusterValues",ArrayType(cluster_value_schema))])

convert_filter_values_udf = udf(convert_filter_values,cluster_details_schema)

# print convert_filter_values({'v': 1},'f')
print convert_filter_values({'v1_aserr': 4,'v2': 3,'v3': 2,'v4': 1},'Type_Length_article_attr')

filtered_qfv_df = filtered_qfv_df.withColumn('cluster_details_struct',(convert_filter_values_udf(col('filter_value_count'),col('filter_name')))).select('user_query','cluster_details_struct')

final_df = filtered_qfv_df.groupBy('user_query').agg(collect_list(col('cluster_details_struct')).alias('cluster_details_arr_struct')).withColumn('data_date', to_date(lit(current_date),'yyyy-MM-dd'))

final_df.write.saveAsTable(name=table_name,format='orc',mode='append',partitionBy=['data_date'],path=s3_base_path+table_name)




