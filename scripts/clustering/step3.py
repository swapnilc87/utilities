import pandas as pd
import csv
import requests
import math
import io
import os
import time
import datetime
import numpy as np
import findspark
findspark.init()
pd.options.display.max_rows = 400
pd.options.display.max_colwidth=200
pd.set_option('display.max_columns', None)
from pyspark.sql.functions import *
from pyspark.sql import *
from pyspark.sql.types import *
from pyspark.sql import functions as f
from pyspark.sql import types as t
import re
import datetime as dt
import numpy as np
from collections import Counter
%matplotlib inline

spark = SparkSession.builder.appName('solr_query_dump').enableHiveSupport().getOrCreate()

current_date = '2019-04-30'
filters_schema = StructType([StructField("id",StringType()),StructField("field",StringType()),StructField("value",StringType())])
cluster_value_schema = StructType([StructField("id",StringType()),StructField("name",StringType()),StructField("filters",ArrayType(filters_schema))])
cluster_details_schema = StructType([StructField("id",StringType()),StructField("name",StringType()),StructField("clusterValues",ArrayType(cluster_value_schema))])

cluster_redis_schema = StructType([StructField("solrQuery",StringType()),StructField("clusterDetails",ArrayType(cluster_details_schema))])


def convert_redis(solrQuery,cluster_details) :
    return [solrQuery,cluster_details]
convert_redis_udf = udf(convert_redis,cluster_redis_schema)


# df = spark.read.csv(path='distinct_queries/genderATSolrQueries.tsv',header=False,inferSchema=True,sep="\t")
# df = df.withColumnRenamed('_c0','user_query').withColumnRenamed('_c1','solr_query').withColumnRenamed('_c2','totalCount')

df=spark.sql("select * from searchdb.filtered_cluster_queries_base")
df

cluster_df = spark.sql("select * from searchdb.query_filter_clusters_monthly where data_date='{current_date}'".format(current_date=current_date))

cluster_df.show(10)

filtered_df = df.join(cluster_df,'user_query',how='inner')
filtered_df

final_df = filtered_df.select('user_query',f.to_json(convert_redis_udf(col('solrQuery'),col('cluster_details_arr_struct'))).alias('redis_value'))

# final_df.show(10,False)

final_df.coalesce(1).write.csv('redis_dump_filtered_30_04_2',sep="\t",escapeQuotes=False)


