
# coding: utf-8

# In[1]:


import pandas as pd
import sys
pd.options.display.max_colwidth=400


# In[2]:


df = pd.read_csv("/Users/300032675/ltr_27_May/profiling.log",header=None,error_bad_lines=False,delimiter='!')
# df = pd.read_csv("/Users/300032675/searchWorkspace/codebase/solr-sprod/solr-7.4.0/server/profiling_old_feature.log",header=None,error_bad_lines=False,delimiter='!')
df.columns=['line']
df


# In[3]:


feature_computation = 'Feature Computation'
model_predict_time = 'Model.predict time'
field_value_scorer = 'FieldValueFeatureScorer'
qd_scorer = 'QDFeatureScorer'
value_feature_scorer = 'org.apache.solr.ltr.feature.Feature'
solr_scorer = 'SolrFeatureScorer'
# df[df.line.str.contains()]


# In[4]:


def extract_time(line) :
    return int(line.split('; ')[-1])


# In[5]:


# extract_time('2018-09-20 09:54:52.790 DEBUG (qtp407697359-50) [c:sprod s:2297 r:core_node12 x:sprod_2297_replica_p11] o.a.s.l.LTRScoringQuery Feature Computation time for a doc; 2')


# In[17]:


#nano time
#/1000 ==> micro
feature_comp_df = df[df.line.str.contains(feature_computation)]
feature_comp_df['fc_time'] = feature_comp_df['line'].apply(lambda x: extract_time(x)/1000)
# feature_comp_df.columns=['fc_time']
print feature_comp_df.fc_time.describe()
print feature_comp_df.fc_time.sum()


# In[7]:


#nano time
model_predict_df = df[df.line.str.contains(model_predict_time)]
model_predict_df['mp_time'] = model_predict_df['line'].apply(lambda x: extract_time(x))
# feature_comp_df.columns=['fc_time']
print model_predict_df.mp_time.describe()
print model_predict_df.mp_time.sum()


# In[8]:


overall_ltr = 'Time for reranking using model'
ltr_df = df[df.line.str.contains(overall_ltr)]
ltr_df['ltr_time'] = ltr_df['line'].apply(lambda x: extract_time(x))
# feature_comp_df.columns=['fc_time']
print ltr_df.ltr_time.describe()
print ltr_df.ltr_time.sum()


# In[9]:


# df = pd.read_csv("/Users/300032675/searchWorkspace/detailed_profiling.txt",header=None,error_bad_lines=False,delimiter='!')
# df.columns=['line']
# df


# In[10]:


# overall_ltr = 'Time for reranking using model'
# ltr_df = df[df.line.str.contains(overall_ltr)]
# ltr_df['ltr_time'] = ltr_df['line'].apply(lambda x: extract_time(x))
# # feature_comp_df.columns=['fc_time']
# ltr_df.ltr_time.describe()


# In[11]:


# feature_comp_df = df[df.line.str.contains(feature_computation)]
# feature_comp_df['fc_time'] = feature_comp_df['line'].apply(lambda x: extract_time(x))
# # feature_comp_df.columns=['fc_time']
# feature_comp_df.fc_time.describe()


# In[12]:


feature_comp_df = df[df.line.str.contains(qd_scorer)]
feature_comp_df['f_time'] = feature_comp_df['line'].apply(lambda x: extract_time(x)/1000)
print feature_comp_df.f_time.describe()
print feature_comp_df.f_time.sum()


# In[13]:


feature_comp_df = df[df.line.str.contains(solr_scorer)]
feature_comp_df['f_time'] = feature_comp_df['line'].apply(lambda x: extract_time(x)/1000)
print feature_comp_df.f_time.describe()
print feature_comp_df.f_time.sum()


# In[14]:


feature_comp_df = df[df.line.str.contains(value_feature_scorer)]
feature_comp_df['f_time'] = feature_comp_df['line'].apply(lambda x: extract_time(x)/1000)
print feature_comp_df.f_time.describe()
print feature_comp_df.f_time.sum()


# In[15]:


feature_comp_df = df[df.line.str.contains(field_value_scorer)]
feature_comp_df['f_time'] = feature_comp_df['line'].apply(lambda x: extract_time(x)/1000)
print feature_comp_df.f_time.describe()
print feature_comp_df.f_time.sum()

