#!/usr/bin/env python

import sys
import csv
import math
import io
import os
import time
import datetime
import numpy as np
from pyspark.sql.functions import *
from pyspark.sql import *
from pyspark.sql.types import *
from pyspark.sql import functions as f
from pyspark.sql import types as t
import re
import datetime as dt

spark = SparkSession.builder.config("spark.executor.cores",2).config("spark.executor.memory","4000m").appName('ltr_analysis_'+sys.argv[1]+'_'+str(sys.argv[2])).enableHiveSupport().getOrCreate()



# In[4]:


# sc=spark.sparkContext
# hadoop_conf=sc._jsc.hadoopConfiguration()
# hadoop_conf.set("fs.s3n.impl", "org.apache.hadoop.fs.s3native.NativeS3FileSystem")
# hadoop_conf.set("fs.s3n.awsAccessKeyId", access_id)
# hadoop_conf.set("fs.s3n.awsSecretAccessKey", access_key)


# In[ ]:


# spark.read.json('s3://myntra-datasciences/events/2019/05/user_event_08_05_2019/raw/part-00001.gz')


# In[ ]:





# In[ ]:





# In[5]:


table_name='searchdb.session_stitched_raw_daily_ltr_zero'
s3_base_path="s3://myntra/searchteam/hive/"
current_date = '2019-05-08'
current_date = sys.argv[1]
total_days = 1
maxx = 24

now_dt = dt.datetime.strptime(current_date,'%Y-%m-%d')
year = now_dt.year
month = now_dt.month
day = now_dt.day
if day< 10 :
    day ='0'+str(day)
if month < 10 :
    month = '0'+str(month)
hr = int(sys.argv[2])
# hr=20
if hr < 10 :
     hr = '0'+str(hr)

print year,month,day,hr
input_path = 's3://madlytics/flattenedDataOrc/data=eventsTable/y={}/m={}/d={}/h={}/{}/'.format(year,month,day,hr,'min=*')
print input_path,dt.datetime.now()
date_today = dt.datetime.strftime(now_dt,'%Y-%m-%d')
#         print date_today

events_df = spark.read.orc(input_path).select('event_type','screen_name','custom_variable_1','ab_tests','load_date')
events_df


# In[6]:


zero_searches_df = events_df.where("event_type = 'ScreenLoad' and custom_variable_1='0'").where("screen_name like '%Shopping Page-List%' OR screen_name like '%Shopping Page-Search%'").withColumn('list_type',when(f.expr("screen_name like '%Shopping Page-Search%'"),lit('search')).otherwise('list')).withColumn('lane',f.regexp_extract(col('ab_tests'), '"LTR_GS":"([A-Za-z0-9]+)",',1))




# In[7]:


aggregated_df = zero_searches_df.groupBy('load_date','list_type','lane').agg(f.count(col('custom_variable_1')).alias('zero_searches')).withColumn('data_date',f.lit(current_date)).withColumn('hr',f.lit(hr))


# In[8]:


aggregated_df.coalesce(1).write.saveAsTable(name=table_name,format='orc',mode='append',partitionBy=['load_date','data_date'],path=s3_base_path+table_name)
