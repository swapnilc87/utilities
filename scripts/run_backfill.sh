#!/bin/bash

start='2019-05-01'
end='2019-05-03'

start=$(date -d $start +%Y%m%d)
end=$(date -d $end +%Y%m%d)

while [[ $start -le $end ]]
do
        echo $start
        start=$(date -d"$start + 1 day" +"%Y%m%d")
	./run_single_day.sh $start
done

