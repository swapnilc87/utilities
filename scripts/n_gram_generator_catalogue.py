#!/usr/bin/env python
# coding: utf-8

# In[70]:


#Python notebook to create bigrams, trigrams and unigrams and create json files to upload to Solr
import nltk
# nltk.download('stopwords')
import pandas as pd
from nltk import word_tokenize
from nltk.util import ngrams
from collections import Counter
from nltk import everygrams
from nltk import FreqDist
from nltk.corpus import words
total=FreqDist()
import numpy as np
from nltk.corpus import stopwords
stopwords = stopwords.words('english')
import csv
import json
pd.options.display.max_colwidth=400
pd.options.display.max_rows=500


from pyspark.sql import SparkSession
from pyspark.sql.functions import explode, desc
from pyspark.ml.feature import NGram
from pyspark.sql import functions as f
from pyspark.sql.functions import *

spark = SparkSession.builder.appName('lexicon').config("spark.executor.memory","8000m").getOrCreate()


dim_product_df = spark.read.parquet("s3://madlytics/udpAggregates/sources/bidb.dim_product/").select('style_id','master_category','article_type','brand','base_colour','age_group','gender','season','style_name','list_display_name','catalog_live_date','style_status')
dim_product_df  = dim_product_df.filter(f.col('catalog_live_date') > 20170101)
dim_product_df


all_columns = dim_product_df.columns
for column_name in all_columns :
    dim_product_df = dim_product_df.withColumn(column_name, f.lower(col(column_name)))

dim_product_df


dim_product_df.filter(f.col('style_name').like('%huesen%') | f.col('style_name').like('% pent %') | f.col('style_name').like('%plazzo%')).select('style_id','style_status','style_name','catalog_live_date').limit(500).toPandas()

all_term_count_df = dim_product_df.groupBy('master_category').agg(f.countDistinct('style_id').alias('impressions')).withColumnRenamed('master_category','attribute_value')
for column_name in all_columns :
    if 'master_category' == column_name or 'style_id' == column_name:
        continue
    temp_df = dim_product_df.groupBy(column_name).agg(f.countDistinct('style_id').alias('impressions')).withColumnRenamed(column_name,'attribute_value')
    all_term_count_df = all_term_count_df.union(temp_df)

# all_term_count_df

dim_product_attributes_df = spark.read.csv('s3://myntra/searchteam/ddp_table_dumps/dim_product_attributes')
attr_columns=['style_id','attribute_type','attribute_value','last_modified_on','platform_id','bi_last_modified_on']

# dim_product_attributes_df = dim_product_attributes_df.withColumn('attribute_value',f.lower(f.col('attribute_value')))

i=0
for col_name in attr_columns :
    dim_product_attributes_df = dim_product_attributes_df.withColumnRenamed('_c'+str(i),col_name)
    i+=1
dim_product_attributes_df
dim_product_attributes_df = dim_product_attributes_df.withColumn('attribute_value',f.lower(f.col('attribute_value')))


dim_product_attributes_df.where("attribute_value like '%-%' ").show(100)

dim_product_attributes_df = dim_product_attributes_df.join(dim_product_df,on='style_id',how='inner').select('attribute_value','style_id')

product_attr_grouped_df = dim_product_attributes_df.groupBy(f.col('attribute_value')).agg(f.countDistinct(f.col('style_id')).alias('impressions'))

## can be sum or max depending on how we want to comsume it
grouped_attr_val_count_df = all_term_count_df.union(product_attr_grouped_df)
# grouped_attr_val_count_df = grouped_attr_val_count_df.withColumn('attribute_value',f.trim(f.col('attribute_value'),'[,.]+',''))
grouped_attr_val_count_df = grouped_attr_val_count_df.groupBy('attribute_value').agg(f.sum(f.col('impressions')).alias('impressions')).withColumn('words',f.split(f.col('attribute_value'),',?\.?\s+'))
grouped_attr_val_count_df

unigram_transformer = NGram(n=1, inputCol="words", outputCol="unigrams") #find unigrams
bigram_transformer = NGram(n=2, inputCol="words", outputCol="bigrams")  # find bigrams
trigram_transformer = NGram(n=3, inputCol="words", outputCol="trigrams")   # find trigrams

unigram_df = unigram_transformer.transform(grouped_attr_val_count_df.dropna())
bigram_df = bigram_transformer.transform(grouped_attr_val_count_df.dropna())
trigram_df = trigram_transformer.transform(grouped_attr_val_count_df.dropna())

unigram_df


# In[39]:


unigrams_agg_df=unigram_df.select(explode("unigrams").alias("unigram"),'impressions').groupBy("unigram").agg(f.sum(f.col('impressions')).alias('impressions'))
bigrams_agg_df=bigram_df.select(explode("bigrams").alias("bigram"),'impressions').groupBy("bigram").agg(f.sum(f.col('impressions')).alias('impressions'))
trigrams_agg_df=trigram_df.select(explode("trigrams").alias("trigrams"),'impressions').groupBy("trigrams").agg(f.sum(f.col('impressions')).alias('impressions'))


# In[40]:


unigrams_agg_df = unigrams_agg_df.dropna()
unigrams_agg_df = unigrams_agg_df.filter(f.col('impressions')>12)


bigrams_agg_df = bigrams_agg_df.dropna()
bigrams_agg_df = bigrams_agg_df.filter(f.col('impressions')>12)


trigrams_agg_df = trigrams_agg_df.dropna()
trigrams_agg_df = trigrams_agg_df.filter(f.col('impressions')>12)


# In[41]:


unigrams_agg_df.coalesce(1).write.mode("overwrite").csv("s3://myntra/searchteam/sanity_corpus/unigrams")


# In[42]:


bigrams_agg_df.coalesce(1).write.mode("overwrite").csv("s3://myntra/searchteam/sanity_corpus/bigrams")


# In[43]:


trigrams_agg_df.coalesce(1).write.mode("overwrite").csv("s3://myntra/searchteam/sanity_corpus/trigrams")
